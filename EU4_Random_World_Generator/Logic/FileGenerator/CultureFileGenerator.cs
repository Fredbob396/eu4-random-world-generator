﻿using System;
using System.IO;

namespace EU4_Random_World_Generator
{
    class CultureFileGenerator
    {
        private static CultureFileGenerator _instance = new CultureFileGenerator();

        private CultureFileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static CultureFileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the culture file generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating culture files...");
            GenerateCultures();
        }

        /// <summary>
        /// Generates the culture file
        /// </summary>
        private void GenerateCultures()
        {
            string cultureFileDirectory = $@"{R.ModDirectory}\common\cultures";
            string cultureFile = cultureFileDirectory + @"\generated_cultures.txt";

            if (!Directory.Exists(cultureFileDirectory))
            {
                Directory.CreateDirectory(cultureFileDirectory);
            }

            using (TextWriter textWriter = new StreamWriter(cultureFile))
            {
                var writer = new EU4FileWriter(textWriter);
                foreach (CultureGroup cultureGroup in World.CultureGroups)
                {
                    writer.WriteBraceTag(cultureGroup.InternalName);
                    writer.WriteVariableTag("graphical_culture", cultureGroup.TechGroup.Gfx);

                    writer.WriteBraceTag("male_names");
                    foreach (string maleName in cultureGroup.MaleNames)
                    {
                        writer.WritePropertyTag(maleName);
                    }
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("female_names");
                    foreach (string femaleName in cultureGroup.FemaleNames)
                    {
                        writer.WritePropertyTag(femaleName);
                    }
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("dynasty_names");
                    foreach (string surname in cultureGroup.Surnames)
                    {
                        writer.WritePropertyTag($"\"{surname}\"");
                    }
                    writer.WriteClosingTag();

                    foreach (Culture culture in cultureGroup.Cultures)
                    {
                        writer.WriteBraceTag(culture.InternalName);
                        writer.WriteClosingTag();
                    }

                    writer.WriteClosingTag();
                }
            }
        }
    }
}
