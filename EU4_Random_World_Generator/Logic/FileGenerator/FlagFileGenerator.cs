﻿using System;
using EU4FlagLib;

namespace EU4_Random_World_Generator
{
    class FlagFileGenerator
    {
        private static FlagFileGenerator _instance = new FlagFileGenerator();

        private FlagFileGenerator() { }

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static FlagFileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the flag files generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating flag files...");
            GenerateFlagFiles();
        }

        /// <summary>
        /// Generates a flag for every country
        /// </summary>
        private void GenerateFlagFiles()
        {
            var maker = new FlagMaker(R.Random);
            foreach (Country country in World.Countries)
            {
                maker.CreateFlag(country.TechGroup.FlagComponents, country.Tag, R.ModDirectory);
            }
        }
    }
}
