﻿using System.IO;

namespace EU4_Random_World_Generator
{
    class EU4FileWriter
    {
        private readonly TextWriter _writer;
        private int _braceDepth;

        public EU4FileWriter(TextWriter streamWriter)
        {
            _writer = streamWriter;
        }

        #region Regular
        /// <summary>
        /// Writes a brace tag
        /// </summary>
        /// <param name="tag">The tag string</param>
        public void WriteBraceTag(string tag)
        {
            _writer.WriteLine($"{GetCurrentIndentation()}{tag} = {{");
            _braceDepth++;
        }

        /// <summary>
        /// Writes a variable tag
        /// </summary>
        /// <param name="tag">The tag string</param>
        /// <param name="variable"></param>
        public void WriteVariableTag(string tag, string variable)
        {
            _writer.WriteLine($"{GetCurrentIndentation()}{tag} = {variable}");
        }

        /// <summary>
        /// Writes a variable tag
        /// </summary>
        /// <param name="tag">The tag string</param>
        /// <param name="variable"></param>
        public void WriteVariableTag(string tag, int variable)
        {
            _writer.WriteLine($"{GetCurrentIndentation()}{tag} = {variable}");
        }

        /// <summary>
        /// Writes a property tag
        /// </summary>
        /// <param name="tag">The tag string</param>
        public void WritePropertyTag(string tag)
        {
            _writer.WriteLine($"{GetCurrentIndentation()}{tag}");
        }

        /// <summary>
        /// Writes a closing brace
        /// </summary>
        public void WriteClosingTag()
        {
            _braceDepth--;
            _writer.WriteLine($"{GetCurrentIndentation()}}}");
        }
        #endregion

        #region Localisation
        /// <summary>
        /// Writes a localisation header
        /// </summary>
        /// <param name="language"></param>
        public void WriteLocalisationHeader(string language)
        {
            _writer.WriteLine($"l_{language}:");
        }

        /// <summary>
        /// Writes a localisation tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTag(string tag, string variable)
        {
            _writer.WriteLine($" {tag}:0 \"{variable}\"");
        }

        /// <summary>
        /// Writes a localisation tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTag(int tag, string variable)
        {
            _writer.WriteLine($" {tag}:0 \"{variable}\"");
        }

        /// <summary>
        /// Writes a localisation adjective tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTagAdj(string tag, string variable)
        {
            _writer.WriteLine($" {tag}_ADJ:0 \"{variable}\"");
        }

        /// <summary>
        /// Writes a localisation adjective tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTagAdj(int tag, string variable)
        {
            _writer.WriteLine($" {tag}_ADJ:0 \"{variable}\"");
        }
        #endregion

        #region Province
        /// <summary>
        /// Writes a localisation province tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTagProv(int tag, string variable)
        {
            _writer.WriteLine($" PROV{tag}:0 \"{variable}\"");
        }

        /// <summary>
        /// Writes a localisation province adjective tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="variable"></param>
        public void WriteLocalisationTagProvAdj(int tag, string variable)
        {
            _writer.WriteLine($" PROV_ADJ{tag}:0 \"{variable}\"");
        }
        #endregion

        /// <summary>
        /// Gets the current indentation given the brace depth
        /// </summary>
        /// <returns>The current indentation given the brace depth</returns>
        public string GetCurrentIndentation()
        {
            string indentation = "";
            for (int i = 0; i < _braceDepth; i++)
            {
                indentation += "\t";
            }
            return indentation;
        }
    }
}
