﻿using System;
using System.IO;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class ReligionFileGenerator
    {
        private static ReligionFileGenerator _instance = new ReligionFileGenerator();

        private ReligionFileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static ReligionFileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the religion file generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating religion files...");
            GenerateReligions();
        }

        /// <summary>
        /// Generates the religion file
        /// </summary>
        private void GenerateReligions()
        {
            string religionFileDirectory = $@"{R.ModDirectory}\common\religions";
            string religionFileName = religionFileDirectory + @"\generated_religions.txt";

            var hreEmperor = (
                from c in World.Countries
                where c.IsHreEmperor
                select c).First();

            if (!Directory.Exists(religionFileDirectory))
            {
                Directory.CreateDirectory(religionFileDirectory);
            }

            using (TextWriter textWriter = new StreamWriter(religionFileName))
            {
                var writer = new EU4FileWriter(textWriter);

                foreach (ReligionGroup religionGroup in World.ReligionGroups)
                {
                    writer.WriteBraceTag(religionGroup.InternalName);
                    foreach (Religion religion in religionGroup.Religions)
                    {
                        writer.WriteBraceTag(religion.InternalName);

                        writer.WriteBraceTag("color");
                        writer.WritePropertyTag(religion.GetRgb());
                        writer.WriteClosingTag();

                        writer.WriteVariableTag("icon", religion.Icon);

                        writer.WriteBraceTag("country");
                        writer.WriteVariableTag("tolerance_own", "1");
                        writer.WriteVariableTag("tolerance_heretic", "1");
                        writer.WriteClosingTag();

                        if (hreEmperor.Religion.Equals(religion))
                        {
                            writer.WriteVariableTag("hre_religion", "yes");
                        }
                        writer.WriteBraceTag("heretic");
                        foreach (var heretic in religion.Heresies)
                        {
                            writer.WritePropertyTag(heretic.InternalName);
                        }
                        writer.WriteClosingTag();
                        writer.WriteClosingTag();
                    }
                    writer.WriteClosingTag();
                }
            }
        }
    }
}
