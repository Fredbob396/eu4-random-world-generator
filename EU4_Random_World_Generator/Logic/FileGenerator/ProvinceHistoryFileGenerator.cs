﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EU4_Random_World_Generator
{
    class ProvinceHistoryFileGenerator
    {
        private static ProvinceHistoryFileGenerator _instance = new ProvinceHistoryFileGenerator();

        private ProvinceHistoryFileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static ProvinceHistoryFileGenerator Instance()
        {
            return _instance;
        }

        private readonly string _provinceHistoryFileDirectory = $@"{R.ModDirectory}\history\provinces";

        /// <summary>
        /// Runs the province history generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating province history files...");
            GenerateProvinceHistory();
        }

        /// <summary>
        /// Generates the province history files
        /// </summary>
        private void GenerateProvinceHistory()
        {
            if (!Directory.Exists(_provinceHistoryFileDirectory))
            {
                Directory.CreateDirectory(_provinceHistoryFileDirectory);
            }
            List<Province> allProvinces =
                (from sr in World.SuperRegions
                 from r in sr.Regions
                 from a in r.Areas
                 from p in a.Provinces
                 select p).ToList();

            Parallel.ForEach(allProvinces, (province) =>
            {
                string provinceHistoryFile = _provinceHistoryFileDirectory + $@"\{province.Id}.txt";
                using (TextWriter textWriter = new StreamWriter(provinceHistoryFile))
                {
                    var writer = new EU4FileWriter(textWriter);

                    if (province.Owner != null)
                    {
                        writer.WriteVariableTag("owner", province.Owner.Tag);
                        writer.WriteVariableTag("controller", province.Owner.Tag);
                        writer.WriteVariableTag("add_core", province.Owner.Tag);
                    }
                    writer.WriteVariableTag("capital", $"\"{province.Name}\"");
                    if (province.IsCity)
                    {
                        writer.WriteVariableTag("is_city", "yes");
                    }
                    writer.WriteVariableTag("culture", province.Culture.InternalName);
                    writer.WriteVariableTag("religion", province.Religion.InternalName);
                    if (province.InHre)
                    {
                        writer.WriteVariableTag("hre", "yes");
                    }
                    writer.WriteVariableTag("base_tax", province.Adm);
                    writer.WriteVariableTag("base_production", province.Dip);
                    writer.WriteVariableTag("base_manpower", province.Mil);

                    //TODO: Generate Trade Good, either ingame or programatically
                    //writer.WriteVariableTag("trade_goods", "cloth");

                    if (province.HasFort)
                    {
                        writer.WriteVariableTag("fort_15th", "yes");
                    }
                }
            });
        }
    }
}
