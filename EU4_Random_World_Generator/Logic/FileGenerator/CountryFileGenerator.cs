﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace EU4_Random_World_Generator
{
    class CountryFileGenerator
    {
        private static CountryFileGenerator _instance = new CountryFileGenerator();

        private CountryFileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static CountryFileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the common country file generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating country common files...");
            GenerateCommonFiles();
            Console.WriteLine("Generating country history files...");
            GenerateHistoryFiles();
            Console.WriteLine("Generating country tags files...");
            GenerateTagsFile();
        }

        /// <summary>
        /// Generates the country common files
        /// </summary>
        private void GenerateCommonFiles()
        {
            string countryCommonFileDirectory = $@"{R.ModDirectory}\common\countries";
            if (!Directory.Exists(countryCommonFileDirectory))
            {
                Directory.CreateDirectory(countryCommonFileDirectory);
            }
            Parallel.ForEach(World.Countries, country => {
                string countryCommonFile = countryCommonFileDirectory + $@"\{country.Tag}.txt";

                List<string> maleNames = country.PrimaryCulture.ParentCultureGroup.MaleNames;
                List<string> femaleNames = country.PrimaryCulture.ParentCultureGroup.FemaleNames;
                List<string> surnames = country.PrimaryCulture.ParentCultureGroup.Surnames;

                using (TextWriter textWriter = new StreamWriter(countryCommonFile))
                {
                    var writer = new EU4FileWriter(textWriter);

                    writer.WritePropertyTag($"# Country Name: {country.Name}");
                    writer.WriteVariableTag("graphical_culture", country.TechGroup.Gfx);

                    writer.WriteBraceTag("color");
                    writer.WritePropertyTag(country.GetRgb());
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("historical_idea_groups");
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("historical_units");
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("monarch_names");
                    foreach (string name in maleNames)
                    {
                        writer.WriteVariableTag($"\"{name} #0\"", "100");
                    }
                    foreach (string name in femaleNames)
                    {
                        writer.WriteVariableTag($"\"{name} #0\"", "-100");
                    }
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("leader_names");
                    foreach (string surname in surnames)
                    {
                        writer.WritePropertyTag($"\"{surname}\"");
                    }
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("ship_names");
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("army_names");
                    writer.WriteClosingTag();

                    writer.WriteBraceTag("fleet_names");
                    writer.WriteClosingTag();
                }
            });
        }

        /// <summary>
        /// Generates the country history files
        /// </summary>
        private void GenerateHistoryFiles()
        {
            string countryHistoryFileDirectory = $@"{R.ModDirectory}\history\countries";
            if (!Directory.Exists(countryHistoryFileDirectory))
            {
                Directory.CreateDirectory(countryHistoryFileDirectory);
            }

            Parallel.ForEach(World.Countries, country =>
            {
                string countryHistoryFile = countryHistoryFileDirectory + $@"\{country.Tag}.txt";
                using (TextWriter textWriter = new StreamWriter(countryHistoryFile))
                {
                    var writer = new EU4FileWriter(textWriter);
                    writer.WriteVariableTag("government", country.Government.Name);
                    writer.WriteVariableTag("government_rank", country.GovernmentRank);
                    writer.WriteVariableTag("primary_culture", country.PrimaryCulture.InternalName);
                    writer.WriteVariableTag("religion", country.Religion.InternalName);
                    writer.WriteVariableTag("technology_group", country.TechGroup.Name);
                    writer.WriteVariableTag("capital", country.CapitalProvince.Id);
                    if (country.IsHreElector)
                    {
                        writer.WriteVariableTag("elector", "yes");
                    }
                    if (country.UnifiedCultureLeader)
                    {
                        writer.WriteBraceTag("1444.1.1");
                        writer.WriteVariableTag("set_country_flag", "unification_leader");
                        writer.WriteClosingTag();
                    }
                    if (country.UnifiedCultureSubject)
                    {
                        writer.WriteBraceTag("1444.1.1");
                        writer.WriteVariableTag("set_country_flag", "unification_subject");
                        writer.WriteClosingTag();
                    }
                }
            });
        }

        /// <summary>
        /// Generates the country tags file
        /// </summary>
        private void GenerateTagsFile()
        {
            string countryTagsFileDirectory = $@"{R.ModDirectory}\common\country_tags";
            string countryTagsFile = countryTagsFileDirectory + @"\generated_countries.txt";
            if (!Directory.Exists(countryTagsFileDirectory))
            {
                Directory.CreateDirectory(countryTagsFileDirectory);
            }
            using (TextWriter textWriter = new StreamWriter(countryTagsFile, true)) //Set to true so it appends rather than overwrites
            {
                var writer = new EU4FileWriter(textWriter);
                foreach (Country country in World.Countries)
                {
                    writer.WriteVariableTag(country.Tag, $"\"countries/{country.Tag}.txt\"");
                }
            }
        }
    }
}
