﻿using System;
using System.IO;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class DiplomacyFileGenerator
    {
        private static DiplomacyFileGenerator _instance = new DiplomacyFileGenerator();

        private DiplomacyFileGenerator() { }
        private readonly string _diplomacyFileDirectory = $@"{R.ModDirectory}\history\diplomacy";

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static DiplomacyFileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the diplomacy files generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating diplomacy files...");
            GenerateHreDiplomacyFile();
        }

        private void GenerateHreDiplomacyFile()
        {
            string hreDiplomacyFile = _diplomacyFileDirectory + @"\hre.txt";

            Country emperor = (
                    from country in World.Countries
                    where country.IsHreEmperor
                    select country).First();

            if (emperor == null)
            {
                throw new Exception("No HRE Emperor Detected! This shouldn't happen!");
            }

            if (!Directory.Exists(_diplomacyFileDirectory))
            {
                Directory.CreateDirectory(_diplomacyFileDirectory);
            }

            using (TextWriter textWriter = new StreamWriter(hreDiplomacyFile))
            {
                var writer = new EU4FileWriter(textWriter);
                writer.WriteBraceTag("1444.1.1");
                writer.WriteVariableTag("emperor", emperor.Tag);
                writer.WriteClosingTag();
            }
        }
    }
}
