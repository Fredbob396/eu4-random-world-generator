﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace EU4_Random_World_Generator
{
    class FileGenerator
    {
        private static FileGenerator _instance = new FileGenerator();

        private FileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static FileGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the file generation process
        /// </summary>
        public void Run()
        {
            var religionFileGenerator = ReligionFileGenerator.Instance();
            var cultureFileGenerator = CultureFileGenerator.Instance();
            var countryFileGenerator = CountryFileGenerator.Instance();
            var provinceHistoryFileGenerator = ProvinceHistoryFileGenerator.Instance();
            var localisationFileGenerator = LocalisationFileGenerator.Instance();
            var diplomacyFileGenerator = DiplomacyFileGenerator.Instance();
            var flagFileGenerator = FlagFileGenerator.Instance();

            Parallel.Invoke(
                religionFileGenerator.Run,
                cultureFileGenerator.Run,
                countryFileGenerator.Run,
                provinceHistoryFileGenerator.Run,
                localisationFileGenerator.Run,
                diplomacyFileGenerator.Run,
                flagFileGenerator.Run,
                GenerateModFile,
                GenerateSeedFile,
                CopyGameFiles
            );
        }
        
        /// <summary>
        /// Generates the mod file
        /// </summary>
        private void GenerateModFile()
        {
            Console.WriteLine("Generating mod file...");
            string modFileString = File.ReadAllText(@"ModFile\modfile.mod");
            modFileString = modFileString.Replace("@MODNAME", R.ModDirectory);
            File.WriteAllText($@"{R.ModDirectory}.mod", modFileString);
        }

        /// <summary>
        /// Generates the seed file
        /// </summary>
        private void GenerateSeedFile()
        {
            Console.WriteLine("Generating seed file...");
            File.WriteAllText($@"{R.ModDirectory}\seed.txt", R.Seed);
        }

        /// <summary>
        /// Copies the game files from the GameFiles folder to the mod folder
        /// </summary>
        private void CopyGameFiles()
        {
            Console.WriteLine("Copying game files...");
            const string source = @"GameFiles";
            new Microsoft.VisualBasic.Devices.Computer()
                .FileSystem.CopyDirectory(source, R.ModDirectory);
        }
    }
}
