﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EU4_Random_World_Generator
{
    class LocalisationFileGenerator
    {
        private static LocalisationFileGenerator _instance = new LocalisationFileGenerator();

        private LocalisationFileGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static LocalisationFileGenerator Instance()
        {
            return _instance;
        }

        private readonly string _localisationDirectory = $@"{R.ModDirectory}\localisation";

        /// <summary>
        /// Runs the localisation file generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating localisation file...");
            GenerateLocalisation();
        }

        /// <summary>
        /// Generates the localisation file
        /// </summary>
        private void GenerateLocalisation()
        {
            string localisationFile = _localisationDirectory + @"\generated_l_english.yml";
            if (!Directory.Exists(_localisationDirectory))
            {
                Directory.CreateDirectory(_localisationDirectory);
            }

            Encoding utf8Bom = new UTF8Encoding(true);
            using (var textWriter = new StreamWriter(File.Open(localisationFile, FileMode.Create), utf8Bom))
            {
                var writer = new EU4FileWriter(textWriter);

                writer.WriteLocalisationHeader("english");

                foreach (Country country in World.Countries)
                {
                    writer.WriteLocalisationTag(country.Tag, country.Name);
                    writer.WriteLocalisationTagAdj(country.Tag, country.Demonym);
                }
                foreach (ReligionGroup religionGroup in World.ReligionGroups)
                {
                    writer.WriteLocalisationTag(religionGroup.InternalName, religionGroup.Name);
                    foreach (Religion religion in religionGroup.Religions)
                    {
                        writer.WriteLocalisationTag(religion.InternalName, religion.Name);
                        foreach (var heresy in religion.Heresies)
                        {
                            writer.WriteLocalisationTag(heresy.InternalName, heresy.Name);
                        }
                    }
                }
                foreach (CultureGroup cultureGroup in World.CultureGroups)
                {
                    writer.WriteLocalisationTag(cultureGroup.InternalName, cultureGroup.Name);
                    foreach (Culture culture in cultureGroup.Cultures)
                    {
                        writer.WriteLocalisationTag(culture.InternalName, culture.Name);
                    }
                }

                List<Province> allProvinces = (
                     from sr in World.SuperRegions
                     from r in sr.Regions
                     from a in r.Areas
                     from p in a.Provinces
                     select p).ToList();

                foreach (Province province in allProvinces)
                {
                    writer.WriteLocalisationTagProv(province.Id, province.Name);
                    writer.WriteLocalisationTagProvAdj(province.Id, province.Demonym);
                }
            }
        }
    }
}
