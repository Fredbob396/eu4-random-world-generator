﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class DatabaseLoader
    {
        private static readonly DatabaseLoader _instance = new DatabaseLoader();

        private SQLiteConnection _dbConnect;

        private int prefix = 0;
        private int first = 1;
        private int second = 2;
        private int last = 3;
        private string male = "M";
        private string female = "F";

        private DatabaseLoader() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static DatabaseLoader Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Starts each database loading process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Loading Data.db...");
            using (_dbConnect = new SQLiteConnection(@"Data Source=Data\Data.db"))
            {
                _dbConnect.Open();
                GetSuperRegionAdjacencies();
                GetRegionAdjacencies();
                GetCulturePlaceNames();
                GetCulturePersonNames();
                GetSuffixes();
            }
        }

        /// <summary>
        /// Retruns the result of the input command as a string array
        /// </summary>
        /// <param name="command">The SQL Command</param>
        /// <returns>The result of the input command as a string array</returns>
        private string[] GetDataArray(SQLiteCommand command)
        {
            List<string> dataList = new List<string>();
            using (command)
            {
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        dataList.Add(reader.GetString(0));
                    }
                }
            }
            string[] dataArray = dataList.ToArray();
            return dataArray;
        }

        /// <summary>
        /// Gets the adjacencies of each Super Region
        /// </summary>
        private void GetSuperRegionAdjacencies()
        {
            foreach (SuperRegion superRegion in World.SuperRegions)
            {
                var getSrAdj = new SQLiteCommand(SQL.GetSuperRegionAdjacenciesSql, _dbConnect);
                getSrAdj.Parameters.AddWithValue("@SUPERREGION", superRegion.Name);
                string[] adjacencies = GetDataArray(getSrAdj);

                foreach (string adjacency in adjacencies)
                {
                    SuperRegion matchingSuperRegion = (
                        from sr in World.SuperRegions
                        where sr.Name == adjacency
                        select sr).FirstOrDefault();
                    
                    if(matchingSuperRegion != null)
                    {
                        superRegion.Adjacencies.Add(matchingSuperRegion);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the adjacencies of each Region
        /// </summary>
        private void GetRegionAdjacencies()
        {
            foreach (Region region in World.SuperRegions.SelectMany(sr => sr.Regions))
            {
                var getRAdj = new SQLiteCommand(SQL.GetRegionAdjacenciesSql, _dbConnect);
                getRAdj.Parameters.AddWithValue("@REGION", region.Name);
                string[] adjacencies = GetDataArray(getRAdj);

                foreach (string adjacency in adjacencies)
                {
                    Region matchingRegion = (
                        from sr in World.SuperRegions
                        from r in sr.Regions
                        where r.Name == adjacency
                        select r).FirstOrDefault();

                    if(matchingRegion != null)
                    {
                        region.Adjacencies.Add(matchingRegion);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a culture type and loads Place Name Syllables for each type in the database
        /// </summary>
        private void GetCulturePlaceNames()
        {
            var getCt = new SQLiteCommand(SQL.GetCultureTypesSql, _dbConnect);
            string[] cultureTypes = GetDataArray(getCt);

            foreach (string cultureType in cultureTypes)
            {
                World.CultureTypes.Add(new CultureType()
                {
                    Name = cultureType
                });
            }
            foreach (CultureType cultureType in World.CultureTypes)
            {
                GetPlaceNames(cultureType);
            }
        }

        /// <summary>
        /// Gets people names for each culture type
        /// </summary>
        private void GetCulturePersonNames()
        {
            foreach (CultureType cultureType in World.CultureTypes)
            {
                GetPeopleNames(cultureType);
            }
        }

        /// <summary>
        /// Gets place names for each culture type
        /// </summary>
        /// <param name="cultureType">The culture type</param>
        private void GetPlaceNames(CultureType cultureType)
        {
            var getPrefixes = new SQLiteCommand(SQL.GetCulturePlaceNamesSql, _dbConnect);
            getPrefixes.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getPrefixes.Parameters.AddWithValue("@ORDER", prefix);

            var getFirst = new SQLiteCommand(SQL.GetCulturePlaceNamesSql, _dbConnect);
            getFirst.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getFirst.Parameters.AddWithValue("@ORDER", first);

            var getSecond = new SQLiteCommand(SQL.GetCulturePlaceNamesSql, _dbConnect);
            getSecond.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getSecond.Parameters.AddWithValue("@ORDER", second);

            var getLast = new SQLiteCommand(SQL.GetCulturePlaceNamesSql, _dbConnect);
            getLast.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getLast.Parameters.AddWithValue("@ORDER", last);

            cultureType.PlaceSyllablesPrefix.AddRange(GetDataArray(getPrefixes));
            cultureType.PlaceSyllablesFirst.AddRange(GetDataArray(getFirst));
            cultureType.PlaceSyllablesMiddle.AddRange(GetDataArray(getSecond));
            cultureType.PlaceSyllablesLast.AddRange(GetDataArray(getLast));
        }

        /// <summary>
        /// Gets people names for each culture type
        /// </summary>
        /// <param name="cultureType">The culture type</param>
        private void GetPeopleNames(CultureType cultureType)
        {
            var getMalePrefixes = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getMalePrefixes.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getMalePrefixes.Parameters.AddWithValue("@GENDER", male);
            getMalePrefixes.Parameters.AddWithValue("@ORDER", prefix);

            var getMaleFirst = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getMaleFirst.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getMaleFirst.Parameters.AddWithValue("@GENDER", male);
            getMaleFirst.Parameters.AddWithValue("@ORDER", first);

            var getMaleSecond = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getMaleSecond.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getMaleSecond.Parameters.AddWithValue("@GENDER", male);
            getMaleSecond.Parameters.AddWithValue("@ORDER", second);

            var getMaleLast = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getMaleLast.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getMaleLast.Parameters.AddWithValue("@GENDER", male);
            getMaleLast.Parameters.AddWithValue("@ORDER", last);

            var getFemalePrefixes = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getFemalePrefixes.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getFemalePrefixes.Parameters.AddWithValue("@GENDER", female);
            getFemalePrefixes.Parameters.AddWithValue("@ORDER", prefix);

            var getFemaleFirst = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getFemaleFirst.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getFemaleFirst.Parameters.AddWithValue("@GENDER", female);
            getFemaleFirst.Parameters.AddWithValue("@ORDER", first);

            var getFemaleSecond = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getFemaleSecond.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getFemaleSecond.Parameters.AddWithValue("@GENDER", female);
            getFemaleSecond.Parameters.AddWithValue("@ORDER", second);

            var getFemaleLast = new SQLiteCommand(SQL.GetCulturePeopleNamesSql, _dbConnect);
            getFemaleLast.Parameters.AddWithValue("@CULTURE", cultureType.Name);
            getFemaleLast.Parameters.AddWithValue("@GENDER", female);
            getFemaleLast.Parameters.AddWithValue("@ORDER", last);

            cultureType.MaleNameSyllablesPrefix.AddRange(GetDataArray(getMalePrefixes));
            cultureType.MaleNameSyllablesFirst.AddRange(GetDataArray(getMaleFirst));
            cultureType.MaleNameSyllablesMiddle.AddRange(GetDataArray(getMaleSecond));
            cultureType.MaleNameSyllablesLast.AddRange(GetDataArray(getMaleLast));

            cultureType.FemaleNameSyllablesPrefix.AddRange(GetDataArray(getFemalePrefixes));
            cultureType.FemaleNameSyllablesFirst.AddRange(GetDataArray(getFemaleFirst));
            cultureType.FemaleNameSyllablesMiddle.AddRange(GetDataArray(getFemaleSecond));
            cultureType.FemaleNameSyllablesLast.AddRange(GetDataArray(getFemaleLast));
        }

        /// <summary>
        /// Gets the suffexes from the databse
        /// </summary>
        private void GetSuffixes()
        {
            using (var command = new SQLiteCommand(SQL.GetSuffixesSql, _dbConnect))
            {
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        World.SuffixPairs.Add(new SuffixPair
                        {
                            Ending = reader.GetString(0),
                            Suffix = reader.GetString(1)
                        });
                    }
                }
            }
        }
    }
}
