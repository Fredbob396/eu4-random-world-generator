﻿namespace EU4_Random_World_Generator
{
    class SQL
    {
        #region Map SQL
        public static string GetSuperRegionAdjacenciesSql =
            "SELECT SR_Adjacencies.SR2 " +
            "FROM SR_Adjacencies " +
            "WHERE SR_Adjacencies.SR1 = @SUPERREGION";

        public static string GetRegionAdjacenciesSql =
            "SELECT R_Adjacencies.R2 " +
            "FROM R_Adjacencies " +
            "WHERE R_Adjacencies.R1 = @REGION";
        #endregion

        #region Names SQL
        public static string GetCultureTypesSql = 
            "SELECT DISTINCT [Culture] " +
            "FROM [PersonNames]";

        public static string GetCulturePlaceNamesSql = 
            "SELECT [Syllable] " +
            "FROM [PlaceNames] " +
            "WHERE [Culture] = @CULTURE " +
            "AND [Order] = @ORDER";

        public static string GetCulturePeopleNamesSql =
            "SELECT [Syllable] " +
            "FROM [PersonNames] " +
            "WHERE [Culture] = @CULTURE " +
            "AND [Gender] = @GENDER " +
            "AND [Order] = @ORDER";
        #endregion

        public static string GetSuffixesSql =
            "SELECT Suffix.Ending, Suffix.Suffix " +
            "FROM Suffix";
    }
}
