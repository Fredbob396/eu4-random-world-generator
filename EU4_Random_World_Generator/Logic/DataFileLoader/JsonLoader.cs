﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EU4FlagLib;
using Newtonsoft.Json;

namespace EU4_Random_World_Generator
{
    class JsonLoader
    {
        private static JsonLoader _instance = new JsonLoader();

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static JsonLoader Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Starts the JSON loading process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Loading tech...");
            LoadTech();
            //TODO: Find better spot for this...
            AssignFlagTypes();
            Console.WriteLine("Loading governments...");
            LoadGovernments();
            Console.WriteLine("Loading Religion Icons...");
            LoadReligionIcons();
        }

        /// <summary>
        /// Loads techs from the JSON file
        /// </summary>
        private void LoadTech()
        {
            string techJson = File.ReadAllText(@"Data\tech.json");
            World.TechGroups = JsonConvert.DeserializeObject<List<Tech>>(techJson);
        }

        private void AssignFlagTypes()
        {
            List<FlagComponents> usedComponents = new List<FlagComponents>();
            foreach (var techGroup in World.TechGroups)
            {
                var unusedComponents = World.FlagComponents
                    .Where(x => !usedComponents.Contains(x)).ToList();

                if (unusedComponents.Count > 0)
                {
                    //Get random flag components from unused components list
                    int componentsIndex = R.Random.Next(0, unusedComponents.Count);
                    FlagComponents components = unusedComponents[componentsIndex];
                    techGroup.FlagComponents = components;
                    usedComponents.Add(components);
                }
                else
                {
                    //Get random flag components from global components list
                    int componentsIndex = R.Random.Next(0, World.FlagComponents.Count);
                    FlagComponents components = World.FlagComponents[componentsIndex];
                    techGroup.FlagComponents = components;
                }
            }
        }

        /// <summary>
        /// Loads governments from the JSON file
        /// </summary>
        private void LoadGovernments()
        {
            string govJson = File.ReadAllText(@"Data\governments.json");
            World.Governments = JsonConvert.DeserializeObject<List<Government>>(govJson);
        }

        private void LoadReligionIcons()
        {
            string religionIconJson = File.ReadAllText(@"Data\religionIcons.json");
            World.ReligionIcons = JsonConvert.DeserializeObject<List<ReligionIcon>>(religionIconJson);
        }
    }
}
