﻿using System;
using EU4FlagLib;

namespace EU4_Random_World_Generator
{
    class FlagsLoader
    {
        private static FlagsLoader _instance = new FlagsLoader();

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static FlagsLoader Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Starts the flag component loading process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Loading flag components...");
            LoadFlagComponents();
        }

        /// <summary>
        /// Loads the flag components and assigns it to the global flag components variable
        /// </summary>
        public void LoadFlagComponents()
        {
            FlagComponentsLoader loader = new FlagComponentsLoader();
            World.FlagComponents = loader.LoadFlagComponents();
        }
    }
}
