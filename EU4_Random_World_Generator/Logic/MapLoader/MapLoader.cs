﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace EU4_Random_World_Generator
{
    class MapLoader
    {
        private static MapLoader _instance = new MapLoader();

        private MapLoader() { }

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static MapLoader Instance()
        {
            return _instance;
        }

        private readonly List<Area> _areas = new List<Area>();
        private readonly List<Region> _regions = new List<Region>();
        private readonly List<SuperRegion> _superregions = new List<SuperRegion>();
        private readonly List<Province> _provinces = new List<Province>();

        /// <summary>
        /// Gets the EU4 root directory given the computer's name
        /// DEBUG USE ONLY
        /// </summary>
        /// <returns>The root EU4 directory</returns>
        private static string Directory()
        {
            switch (Environment.MachineName)
            {
                case "PRO-PC":
                    return @"E:/SteamLibrary/SteamApps/common/Europa Universalis IV";
                case "LAPTOP-LDPVCFPJ":
                    return @"C:\Program Files (x86)\Steam\steamapps\common\Europa Universalis IV";
                default:
                    return "";
            }
        }

        private readonly string _areaFile = $@"{Directory()}/map/area.txt";
        private readonly string _regionFile = $@"{Directory()}/map/region.txt";
        private readonly string _superregionFile = $@"{Directory()}/map/superregion.txt";

        /// <summary>
        /// Runs the map loading process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Loading area.txt...");
            LoadAreasFile(_areaFile);
            Console.WriteLine("Loading region.txt...");
            LoadRegionsFile(_regionFile);
            Console.WriteLine("Loading superregion.txt...");
            LoadSuperRegionsFile(_superregionFile);
            Console.WriteLine("Building map relations...");
            BuildRelations();
            Console.WriteLine("Removing empty super regions...");
            PurgeEmptySuperregions();

            World.SuperRegions = _superregions;
        }

        /// <summary>
        /// Parses the areas file
        /// </summary>
        /// <param name="file">The path to the file</param>
        private void LoadAreasFile(string file)
        {
            using (FileStream fileStream = File.OpenRead(file))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();

                        //Strip all comments from the line
                        line = RemoveComments(line);

                        //If the resulting line is blank or is a color node, skip the line
                        if (string.IsNullOrWhiteSpace(line) || line.Contains("color ="))
                        {
                            continue;
                        }

                        //Reads each line until a line with ONLY a closing brace is found
                        if (line.Contains("{"))
                        {
                            //Trims other characters from the area name and creates a new area
                            var newArea = new Area()
                            {
                                Name = TrimName(line)
                            };
                            _areas.Add(newArea);

                            //Continues to search for provinces until some are found.
                            while (line != "}")
                            {
                                line = reader.ReadLine();
                                line = RemoveComments(line);
                                if (string.IsNullOrWhiteSpace(line) || line.Contains("color ="))
                                {
                                    continue;
                                }

                                //If the line contains a closing brace, end the node
                                if (line.Contains("}"))
                                {
                                    break;
                                }

                                //Removes extra spaces, I think
                                line = Regex.Replace(line, @"\s+", " ");

                                //Splits out the string of provinces and adds them to the area
                                string[] provinceArray = line.Split(' ');
                                foreach (string s in provinceArray)
                                {
                                    _provinces.Add(new Province()
                                    {
                                        Id = Convert.ToInt32(s)
                                    });
                                }
                                newArea.ProvinceIds.AddRange(provinceArray);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parses the region file
        /// </summary>
        /// <param name="file">The path to the file</param>
        private void LoadRegionsFile(string file)
        {
            using (FileStream fileStream = File.OpenRead(file))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();

                        //Strip all comments from the line
                        line = RemoveComments(line);

                        //If the resulting line is blank, skip the line
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }

                        //If the line has an open brace, that means that it is the start of a new region node.
                        if (line.Contains("{"))
                        {
                            //Trims other characters from the region name and creates a new area
                            var newRegion = new Region()
                            {
                                Name = TrimName(line)
                            };
                            _regions.Add(newRegion);

                            //Reads each line until a line with ONLY a closing brace is found
                            //This also ignores lines that have a brace plus whitespace, which excludes
                            //The closing bracket of the "areas" node.
                            while (line != "}")
                            {
                                line = reader.ReadLine();
                                line = RemoveComments(line);
                                //If the line is the areas node or contains a closing brace or is blank, skip it
                                if (line.Contains("areas") || line.Contains("}") || string.IsNullOrWhiteSpace(line))
                                {
                                    continue;
                                }
                                newRegion.AreaNames.Add(line.Trim());
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parses the superregions file
        /// </summary>
        /// <param name="file">The path to the file</param>
        private void LoadSuperRegionsFile(string file)
        {
            using (FileStream fileStream = File.OpenRead(file))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();

                        //Strip all comments from the line
                        line = RemoveComments(line);

                        //If the resulting line is blank, skip the line
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }

                        //If the line has an open brace, that means that it is the start of a new region node.
                        if (line.Contains("{"))
                        {
                            //Trims other characters from the superregion name and creates a new area
                            var newSuperRegion = new SuperRegion()
                            {
                                Name = TrimName(line)
                            };
                            _superregions.Add(newSuperRegion);

                            //Reads each line until a line with ONLY a closing brace is found
                            //This also ignores lines that have a brace plus whitespace
                            while (line != "}")
                            {
                                line = reader.ReadLine();
                                line = RemoveComments(line);
                                //If the line contains a closing brace or is blank, skip it
                                if (line.Contains("}") || string.IsNullOrWhiteSpace(line))
                                {
                                    continue;
                                }
                                newSuperRegion.RegionNames.Add(line.Trim());
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds relations between the different map datapoints
        /// </summary>
        private void BuildRelations()
        {
            foreach (SuperRegion superRegion in _superregions)
            {
                foreach (string superRegionRegionName in superRegion.RegionNames)
                {
                    Region matchingRegion =
                        (from r in _regions
                         where r.Name.Equals(superRegionRegionName)
                         select r).First();
                    superRegion.Regions.Add(matchingRegion);
                    matchingRegion.ParentSuperRegion = superRegion;
                }
                superRegion.RegionNames = null;
            }
            foreach (Region region in _regions)
            {
                foreach (string regionAreaName in region.AreaNames)
                {
                    Area matchingArea = (
                        from a in _areas
                        where a.Name.Equals(regionAreaName)
                        select a).First();
                    region.Areas.Add(matchingArea);
                    matchingArea.ParentRegion = region;
                    matchingArea.ParentSuperRegion = region.ParentSuperRegion;
                }
                region.AreaNames = null;
            }
            foreach (Area area in _areas)
            {
                foreach (string areaProvinceId in area.ProvinceIds)
                {
                    Province matchingProvince = (
                        from p in _provinces
                        where p.Id.Equals(Convert.ToInt32(areaProvinceId))
                        select p).First();
                    area.Provinces.Add(matchingProvince);
                    matchingProvince.ParentArea = area;
                    matchingProvince.ParentRegion = area.ParentRegion;
                    matchingProvince.ParentSuperRegion = area.ParentSuperRegion;
                }
                area.ProvinceIds = null;
            }
        }

        /// <summary>
        /// Removes empty superregions from the superregions list
        /// </summary>
        private void PurgeEmptySuperregions()
        {
            _superregions.RemoveAll(sr => sr.Regions.Count == 0);
        }

        /// <summary>
        /// Trims the node name from a line
        /// </summary>
        /// <param name="line">The line to trim the name out of</param>
        /// <returns>The trimmed line</returns>
        private string TrimName(string line)
        {
            int index = line.IndexOf("=", StringComparison.Ordinal);
            if (index > 0)
            {
                line = line.Substring(0, index);
            }
            line = line.Trim();
            return line;
        }

        /// <summary>
        /// Purges comments from a line
        /// </summary>
        /// <param name="line">The line to remove comments from</param>
        /// <returns>The trimmed line</returns>
        private string RemoveComments(string line)
        {
            int index = line.IndexOf('#');
            if (index > -1)
            {
                line = line.Substring(0, index);
            }
            line = line.Trim();
            return line;
        }
    }
}
