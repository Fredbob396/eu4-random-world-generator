﻿namespace EU4_Random_World_Generator
{
    class Development
    {
        /// <summary>
        /// Randomly choses between medium and low development.
        /// </summary>
        /// <returns>Random development array</returns>
        public static int[] GetRandomCapitalDevelopment()
        {
            return Utils.GetProbability(R.HighOwnedProvinceDevelopmentChance)
                ? GetHighDevelopment()
                : GetMediumDevelopment();
        }

        /// <summary>
        /// Randomly choses between medium and low development.
        /// </summary>
        /// <returns>Random development array</returns>
        public static int[] GetRandomDevelopment()
        {
            return Utils.GetProbability(R.MediumUnownedProvinceDevelopmentChance)
                ? GetMediumDevelopment()
                : GetLowDevelopment();
        }

        /// <summary>
        /// Gets a random capital-scale province development ammount
        /// </summary>
        /// <returns>The development values as an int array</returns>
        public static int[] GetHighDevelopment()
        {
	        int adm = R.Random.Next(R.MinHighProvinceAdm, R.MaxHighProvinceAdm + 1);
            int dip = R.Random.Next(R.MinHighProvinceDip, R.MaxHighProvinceDip + 1);
            int mil = R.Random.Next(R.MinHighProvinceMil, R.MaxHighProvinceMil + 1);

            int[] development = { adm, dip, mil };
            return development;
        }

        /// <summary>
        /// Gets a random normal province development ammount
        /// </summary>
        /// <returns>The development values as an int array</returns>
        public static int[] GetMediumDevelopment()
        {
            int adm = R.Random.Next(R.MinMediumProvinceAdm, R.MaxMediumProvinceAdm + 1);
            int dip = R.Random.Next(R.MinMediumProvinceDip, R.MaxMediumProvinceDip + 1);
            int mil = R.Random.Next(R.MinMediumProvinceMil, R.MaxMediumProvinceMil + 1);

            int[] development = { adm, dip, mil };
            return development;
        }

        /// <summary>
        /// Gets a random low province development ammount
        /// </summary>
        /// <returns>The development values as an int array</returns>
        public static int[] GetLowDevelopment()
        {
            int adm = R.Random.Next(R.MinLowProvinceAdm, R.MaxLowProvinceAdm + 1);
            int dip = R.Random.Next(R.MinLowProvinceDip, R.MaxLowProvinceDip + 1);
            int mil = R.Random.Next(R.MinLowProvinceMil, R.MaxLowProvinceMil + 1);

            int[] development = { adm, dip, mil };
            return development;
        }
    }
}
