﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class CountryGenerator
    {
        private static CountryGenerator _instance = new CountryGenerator();
        private int _nextTagIndex;

        private CountryGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static CountryGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Starts the country generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating countries...");
            GenerateCountriesSimple();
            ThinCountries();
            TagCountries();
            SpecialNaming();
            DetermineUnificationLeaders();
        }

        /// <summary>
        /// Generates a country for every province in the world
        /// </summary>
        private void GenerateCountriesSimple()
        {
            foreach (var culture in World.CultureGroups.SelectMany(x => x.Cultures))
            {
                Religion regionReligion = culture.Area.ParentRegion.Religion;
                List<Province> provincesInCultureArea = culture.Area.Provinces;
                foreach (Province t in provincesInCultureArea)
                {
                    GenerateCountrySimple(culture, regionReligion, t);
                }
            }
        }

        /// <summary>
        /// Thins the world's coutries
        /// </summary>
        private void ThinCountries()
        {
            List<Country> toRemove = new List<Country>();
            foreach (CultureGroup cultureGroup in World.CultureGroups)
            {
                var cultueGroupCountries = (
                    from c in World.Countries
                    where c.PrimaryCulture.ParentCultureGroup == cultureGroup
                    select c).ToList();

                int percentToPurge = R.Random.Next(R.MinPercentOfCountriesToPurge, R.MaxPercentOfCountriesToPurge + 1);

                foreach (Country cultueGroupCountry in cultueGroupCountries)
                {
                    if (Utils.GetProbability(percentToPurge))
                    {
                        toRemove.Add(cultueGroupCountry);
                    }
                }
            }
            foreach (Country country in toRemove)
            {
                World.Countries.Remove(country);
                SetProvinceUnowned(country.CapitalProvince);
            }
        }

        /// <summary>
        /// Generates a tag for every country
        /// </summary>
        private void TagCountries()
        {
            foreach (Country country in World.Countries)
            {
                country.Tag = GetNextTag();
            }
        }

        /// <summary>
        /// Specially names countries
        /// </summary>
        private void SpecialNaming()
        {
            foreach (Country country in World.Countries)
            {
                //Only country in culture group
                bool onlyCountryInCultureGroup = (
                    from ctry in World.Countries
                    where ctry.PrimaryCulture.ParentCultureGroup == country.PrimaryCulture.ParentCultureGroup
                    select ctry).ToList().Count == 1;

                if (onlyCountryInCultureGroup)
                {
                    country.Name = country.PrimaryCulture.ParentCultureGroup.UnDemonynmizedName;
                    country.Demonym = country.PrimaryCulture.ParentCultureGroup.Name;
                    continue;
                }

                //Only country in individual culture
                bool onlyCountryInCulture = (
                    from ctry in World.Countries
                    where ctry.PrimaryCulture == country.PrimaryCulture
                    select ctry).ToList().Count == 1;

                if (onlyCountryInCulture)
                {
                    country.Name = country.PrimaryCulture.UnDemonymizedName;
                    country.Demonym = country.PrimaryCulture.Name;
                }
            }
        }

        /// <summary>
        /// Determines which countries should be the leaders of their cultural unions
        /// </summary>
        private void DetermineUnificationLeaders()
        {
            foreach (CultureGroup cultureGroup in World.CultureGroups.Where(c => c.Unified))
            {
                List<Country> cultureCountries = (
                    from c in World.Countries
                    where c.PrimaryCulture.ParentCultureGroup == cultureGroup
                    select c).ToList();

                //This can happen for some reason. Skip it for now, solve later if it becomes an issue
                if (cultureCountries.Count == 0)
                {
                    continue;
                }

                int leaderIndex = R.Random.Next(0, cultureCountries.Count);
                Country leader = cultureCountries[leaderIndex];

                //Sets the leader bool and names the country after it's culture group
                leader.UnifiedCultureLeader = true;
                leader.Name = leader.PrimaryCulture.ParentCultureGroup.UnDemonynmizedName;
                leader.Demonym = leader.PrimaryCulture.ParentCultureGroup.Name;

                foreach (Country cultureCountry in cultureCountries.Where(c => c.UnifiedCultureLeader == false))
                {
                    cultureCountry.UnifiedCultureSubject = true;
                    cultureCountry.Religion = leader.Religion;
                    cultureCountry.CapitalProvince.Religion = leader.Religion;
                }

                //Randomly exempts some countries
                foreach (Country country in cultureCountries.Where(c => c.UnifiedCultureSubject))
                {
                    if (Utils.GetProbability(R.UnificationExemptionChance))
                    {
                        country.UnifiedCultureSubject = false;
                    }
                }
            }
        }

        /// <summary>
        /// Generates a new country
        /// </summary>
        /// <param name="primaryCulture">The new country's primary culture</param>
        /// <param name="religion">The new country's religion</param>
        /// <param name="capital">The new country's capital province</param>
        private void GenerateCountrySimple(Culture primaryCulture, Religion religion, Province capital)
        {
            Country country;
            string name = NameGenerator.GeneratePlaceName(primaryCulture.Type, true);
            Government government = GetRandomGovernment(primaryCulture.ParentCultureGroup.TechGroup);
            World.Countries.Add(country = new Country()
            {
                InHre = primaryCulture.ParentCultureGroup.HolyRomanEmpire,
                Rgb = GetRandomRgb(),
                Name = name,
                Demonym = NameGenerator.GetDemonym(name),
                PrimaryCulture = primaryCulture,
                Religion = religion,
                Government = government,
                GovernmentRank = GetRandomGovernentRank(government),
                TechGroup = primaryCulture.ParentCultureGroup.TechGroup,
                CapitalProvince = capital,
            });
            SetProvinceOwned(capital, country);
        }

        /// <summary>
        /// Sets a province's owner as the given country
        /// </summary>
        /// <param name="province">The province to set the ownership of</param>
        /// <param name="country">The country to set the province owned by</param>
        private void SetProvinceOwned(Province province, Country country)
        {
            int[] capitalDevelopment = country.TechGroup.Primitive
                ? Development.GetMediumDevelopment()
                : Development.GetRandomCapitalDevelopment();
            province.IsCity = true;
            province.HasFort = true;
            province.Owner = country;
            province.Name = Utils.GetProbability(R.CapitalNamedAfterCountryChance)
                ? country.Name
                : NameGenerator.GeneratePlaceName(province.ParentRegion.CultureGroup.Type);
            province.Religion = province.ParentRegion.Religion;
            province.Culture = province.ParentArea.Culture;
            province.SetDevelopment(capitalDevelopment);
        }

        /// <summary>
        /// Removes the owner from a given province
        /// </summary>
        /// <param name="province">The province to remove ownership of</param>
        private void SetProvinceUnowned(Province province)
        {
            province.IsCity = false;
            province.HasFort = false;
            province.Owner = null;
            province.Name = NameGenerator.GeneratePlaceName(province.ParentRegion.CultureGroup.Type);
            province.Demonym = NameGenerator.GetDemonym(province.Name);
            province.Religion = province.ParentRegion.Religion;
            province.Culture = province.ParentArea.Culture;

            province.SetDevelopment(province.ParentSuperRegion.TechGroup.CanMigrate
                ? Development.GetLowDevelopment()
                : Development.GetRandomDevelopment());
        }

        /// <summary>
        /// Creates the next tag in a sequence
        /// </summary>
        /// <returns>The next tag in the sequence</returns>
        private string GetNextTag()
        {
            int number = _nextTagIndex;
            char startChar = 'Z';
            string tag = "";

            while (number > 99)
            {
                startChar--;
                number -= 100;
            }

            switch (number.ToString().Length)
            {
                case 1:
                    tag += startChar + "0" + number;
                    break;
                case 2:
                    tag += startChar.ToString() + number;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _nextTagIndex++;
            return tag;
        }

        /// <summary>
        /// Gets a random RGB value as an int array
        /// </summary>
        /// <returns>An int array with 3 items</returns>
        private int[] GetRandomRgb()
        {
            int red = R.Random.Next(0, 255);
            int green = R.Random.Next(0, 255);
            int blue = R.Random.Next(0, 255);

            int[] rgb = { red, green, blue };
            return rgb;
        }

        /// <summary>
        /// Gets a random government type based on the technology group
        /// </summary>
        /// <param name="tech">The technology group</param>
        /// <returns>A random government type</returns>
        private Government GetRandomGovernment(Tech tech)
        {
            if (tech.CanMigrate)
            {
                return GetGovernmentFromList(World.Governments
                    .Where(g => g.MigratingTribal).ToList());
            }
            if (tech.SteppeNomad)
            {
                return GetGovernmentFromList(World.Governments
                    .Where(g => g.SteppeNomad).ToList());
            }
            if (tech.HasTribalNonMigratoryGovernments && Utils.GetProbability(R.TribalGovernmentChance))
            {
                return GetGovernmentFromList(World.Governments
                    .Where(g => !g.MigratingTribal)
                    .Where(g => !g.SteppeNomad)
                    .Where(g => g.NonMigratingTribal).ToList());
            }
            return GetGovernmentFromList(World.Governments
                .Where(g => !g.MigratingTribal)
                .Where(g => !g.SteppeNomad)
                .Where(g => !g.NonMigratingTribal)
                .Where(g => !g.FreeCity).ToList());
        }

        /// <summary>
        /// Gets a random government from a list of governments
        /// </summary>
        /// <param name="governments">The list of governments</param>
        /// <returns>A random government from a list of governments</returns>
        private Government GetGovernmentFromList(List<Government> governments)
        {
            List<Government> filteredGovernments;

            bool hasRepublics = governments.Count(x => x.Republic) > 0;
            bool hasTheocracies = governments.Count(x => x.Theocracy) > 0;
            bool hasMonarchies = governments
                .Where(x => !x.Republic)
                .Count(x => !x.Theocracy) > 0;

            if (!hasRepublics && !hasTheocracies && !hasMonarchies)
            {
                throw new Exception("Governments list has no republics, theocracies, or monarchies! What the hell?");
            }

            //Calculates the chance of repbulics and theocracies if any exist
            bool isRepublic = hasRepublics && Utils.GetProbability(R.RepublicChance);
            bool isTheocracy = hasTheocracies && Utils.GetProbability(R.TheocracyChance);

            //If the government has no monarchies, force on republics and theocracies if the list has any
            if (!hasMonarchies)
            {
                if (hasRepublics)
                {
                    isRepublic = true;
                }
                if (hasTheocracies)
                {
                    isTheocracy = true;
                }
            }

            //If both end up being true, a coin toss determines which one gets disabled
            if (isRepublic && isTheocracy)
            {
                bool coinFlip = Utils.GetProbability(50);
                if (coinFlip)
                {
                    isTheocracy = false;
                }
                if (!coinFlip)
                {
                    isRepublic = false;
                }
            }

            if (isRepublic)
            {
                filteredGovernments = governments
                    .Where(x => x.Republic).ToList();
            }
            else if (isTheocracy)
            {
                filteredGovernments = governments
                    .Where(x => x.Theocracy).ToList();
            }
            else
            {
                filteredGovernments = governments
                    .Where(x => !x.Republic)
                    .Where(x => !x.Theocracy).ToList();
            }

            bool hasVeryRareGovernments = filteredGovernments.Count(x => x.VeryRare) > 0;
            bool hasRareGovernments = filteredGovernments.Count(x => x.Rare) > 0;

            //Calculates the chance of rare and very rare governments if any exist
            bool isVeryRareGovernment = hasVeryRareGovernments && Utils.GetProbability(R.VeryRareGovernmentChance);
            bool isRareGovernment = hasRareGovernments && Utils.GetProbability(R.RareGovernmentChance);

            //Very rare supercedes rare
            if (isVeryRareGovernment)
            {
                filteredGovernments = filteredGovernments
                    .Where(x => x.VeryRare).ToList();
            }
            else if (isRareGovernment)
            {
                filteredGovernments = filteredGovernments
                    .Where(x => x.Rare).ToList();
            }
            else
            {
                filteredGovernments = filteredGovernments
                    .Where(x => !x.VeryRare)
                    .Where(x => !x.Rare).ToList();
            }

            //Return random government from filtered list
            int governmentIndex = R.Random.Next(0, filteredGovernments.Count);
            return filteredGovernments[governmentIndex];
        }

        /// <summary>
        /// Gets a random government rank
        /// </summary>
        /// <param name="government">The government to get a rank of</param>
        /// <returns>A government rank</returns>
        private int GetRandomGovernentRank(Government government)
        {
            //If fixed rank, just return the "max"
            if (government.MinRank == government.MaxRank)
            {
                return government.MaxRank;
            }
            //Else returns anything between the min and max rank
            return Utils.GetProbability(R.MinGovernmentRankChance) 
                ? government.MinRank 
                : R.Random.Next(government.MinRank + 1, government.MaxRank);
        }
    }
}
