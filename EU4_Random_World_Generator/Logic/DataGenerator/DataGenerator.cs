﻿namespace EU4_Random_World_Generator
{
    class DataGenerator
    {
        private static DataGenerator _instance = new DataGenerator();

        private DataGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static DataGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the entire data generation process
        /// </summary>
        public void Run()
        {
            var techAssigner = TechAssigner.Instance();
            techAssigner.Run();

            var cultureGenerator = CultureGenerator.Instance();
            cultureGenerator.Run();

            var religionGenerator = ReligionGenerator.Instance();
            religionGenerator.Run();

            var countryGenerator = CountryGenerator.Instance();
            countryGenerator.Run();

            var holyRomanEmpireGenerator = HolyRomanEmpireGenerator.Instance();
            holyRomanEmpireGenerator.Run();

            //Runs after the other generators to get proper religion names
            religionGenerator.NameReligions();
            religionGenerator.NameHeresies();
            cultureGenerator.SpecialNaming();
        }
    }
}
