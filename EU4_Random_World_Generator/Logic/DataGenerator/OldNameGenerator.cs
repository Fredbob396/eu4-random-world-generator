﻿using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class OldNameGenerator
    {
        public static List<string> GeneratedCultureGroups = new List<string>();
        public static List<string> GeneratedCultureGroupTypes = new List<string>(); //FOR THE TEMP NAMING SYSTEM ONLY

        public static List<string> GeneratedCultures = new List<string>();

        public static List<string> GeneratedReligionGroups = new List<string>();
        public static List<string> GeneratedReligions = new List<string>();

        public static List<string> GeneratedCountryNames = new List<string>();

        public static List<string> GeneratedNames = new List<string>();
        public static List<string> GeneratedSurnames = new List<string>();

        public static List<string> GeneratedSuperRegionNames = new List<string>();
        public static List<string> GeneratedRegionNames = new List<string>();
        public static List<string> GeneratedAreaNames = new List<string>();
        public static List<string> GeneratedProvinceNames = new List<string>();

        public static string GenerateCultureGroup(string groupType)
        {
            string cultureGroupName = GenerateRandomString();
            while (GeneratedCultureGroups.Contains(cultureGroupName))
            {
                cultureGroupName = GenerateRandomString();
            }
            GeneratedCultureGroups.Add(cultureGroupName);
            return cultureGroupName;
        }

        public static string GenerateCulture(string groupType)
        {
            string cultureName = GenerateRandomString();
            while (GeneratedCultures.Contains(cultureName))
            {
                cultureName = GenerateRandomString();
            }
            GeneratedCultures.Add(cultureName);
            return cultureName;
        }

        public static string GenerateReligionGroup()
        {
            string religionGroupName = GenerateRandomString() + "ism";
            while (GeneratedReligionGroups.Contains(religionGroupName))
            {
                religionGroupName = GenerateRandomString() + "ism";
            }
            GeneratedReligionGroups.Add(religionGroupName);
            return religionGroupName;
        }

        public static string GenerateReligion(string parentReligion)
        {
            string religionName = $"{GenerateRandomString()}_{parentReligion}";
            while (GeneratedReligions.Contains(religionName))
            {
                religionName = $"{GenerateRandomString()}_{parentReligion}";
            }
            GeneratedReligions.Add(religionName);
            return religionName;
        }

        public static string GenerateCountryName(string groupType)
        {
            string countryName = GenerateRandomString();
            while (GeneratedCountryNames.Contains(countryName))
            {
                countryName = GenerateRandomString();
            }
            GeneratedCountryNames.Add(countryName);
            return countryName;
        }

        public static string GenerateName(string groupType)
        {
            string name = GenerateRandomString();
            while (GeneratedNames.Contains(name))
            {
                name = GenerateRandomString();
            }
            GeneratedNames.Add(name);
            return name;
        }

        public static string GenerateSurname(string groupType)
        {
            string surname = GenerateRandomString();
            if (Utils.GetProbability(25))
            {
                surname = "Of " + surname;
            }
            while (GeneratedSurnames.Contains(surname))
            {
                surname = GenerateRandomString();
                if (Utils.GetProbability(25))
                {
                    surname = "Of " + surname;
                }
            }
            GeneratedSurnames.Add(surname);
            return surname;
        }

        public static string GetRandomCultureType()
        {
            string cultureGroupType = GenerateRandomString();
            while (GeneratedCultureGroupTypes.Contains(cultureGroupType))
            {
                cultureGroupType = GenerateRandomString();
            }
            GeneratedCultureGroupTypes.Add(cultureGroupType);
            return cultureGroupType;
        }

        //Temporary method to generate random strings in place of
        //actual names until the random name generator is complete        
        private static string GenerateRandomString()
        {
            
            string randomString = "";

            for (int i = 0; i < R.Random.Next(3, 15); i++)
            {
                string character = $"{(char)('a' + R.Random.Next(0, 26))}";
                randomString += character;
            }

            return Upper(randomString);
        }

        private static string Upper(string input)
        {
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}
