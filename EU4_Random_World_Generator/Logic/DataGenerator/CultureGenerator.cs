﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class CultureGenerator
    {
        private static CultureGenerator _instance = new CultureGenerator();

        private CultureGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static CultureGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the culture generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating culture groups...");
            GenerateCultureGroups();
            Console.WriteLine("Performing culture group overlap calculations...");
            CultureGroupOverlap();
            Console.WriteLine("Generating cultures...");
            GenerateCultures();
            Console.WriteLine("Naming cultures...");
            NameCultures();
            Console.WriteLine("Determining culture unification...");
            DetermineCultureUnification();
        }

        /// <summary>
        /// Generates a culture group for every region per superregion
        /// </summary>
        private void GenerateCultureGroups()
        {
            //Generates a culture group for every region
            foreach (var sr in World.SuperRegions)
            {
                foreach (var r in sr.Regions)
                {
                    var cg = new CultureGroup();
                    cg.InternalName = NameGenerator.GetInternalCultureGroupName();
                    cg.TechGroup = sr.TechGroup;
                    cg.Regions.Add(r);
                    r.CultureGroup = cg;
                    World.CultureGroups.Add(cg);
                }
            }
        }

        /// <summary>
        /// Generates names for all culture groups
        /// </summary>
        private void NameCultures()
        {
            foreach (var cg in World.CultureGroups)
            {
                cg.Type = NameGenerator.GetRandomCultureType();
                string cgBaseName = NameGenerator.GeneratePlaceName(cg.Type);
                string cgName = NameGenerator.GetDemonym(cgBaseName);
                cg.Name = cgName;
                cg.UnDemonynmizedName = cgBaseName;

                for (int i = 0; i < R.PeopleNamesPerCultureGroup; i++)
                {
                    cg.MaleNames.Add(NameGenerator.GeneratePersonName(cg.Type, "Male"));
                    cg.FemaleNames.Add(NameGenerator.GeneratePersonName(cg.Type, "Female"));
                    cg.Surnames.Add(NameGenerator.GeneratePersonName(cg.Type, "Male"));
                }
                NameGenerator.GeneratedNames.Clear();

                foreach (var culture in cg.Cultures)
                {
                    culture.Type = cg.Type;
                    string cultureBaseName = NameGenerator.GeneratePlaceName(culture.Type);
                    string cultureName = NameGenerator.GetDemonym(cultureBaseName);
                    culture.Name = cultureName;
                    culture.UnDemonymizedName = cultureBaseName;
                }
            }
        }

        /// <summary>
        /// Specially names cultures who are the only member of their culture group and have a single country.
        /// </summary>
        public void SpecialNaming()
        {
            foreach (CultureGroup cultureGroup in World.CultureGroups)
            {
                List<Country> cgCountries = World.Countries
                    .Where(x => x.PrimaryCulture.ParentCultureGroup.Equals(cultureGroup)).ToList();

                if (cgCountries.Count() == 1)
                {
                    Country cgCountry = cgCountries[0];
                    cgCountry.PrimaryCulture.Name = cgCountry.PrimaryCulture.ParentCultureGroup.Name;
                    cgCountry.PrimaryCulture.UnDemonymizedName = cgCountry.PrimaryCulture.ParentCultureGroup.UnDemonynmizedName;
                }
            }
        }

        /// <summary>
        /// Randomly causes one culture group to overwrite/overlap a random adjacent region.
        /// </summary>
        private void CultureGroupOverlap()
        {
            List<CultureGroup> cultureGroupsToRemove = new List<CultureGroup>();
            List<CultureGroup> cultureGroupsThatOverlap = new List<CultureGroup>();
            List<Region> overlappedRegions = new List<Region>();

            foreach (CultureGroup cg in World.CultureGroups)
            {
                if (cultureGroupsToRemove.Contains(cg)) continue;

                List<Region> cgAdjacencies = (
                    from r in cg.Regions
                    from adj in r.Adjacencies
                    select adj).ToList();

                if (!Utils.GetProbability(R.CultureGroupOverlapChance)) continue;

                int randomAdjacencyIndex = R.Random.Next(0, cgAdjacencies.Count);
                Region randomAdjacency = cgAdjacencies[randomAdjacencyIndex];

                foreach (CultureGroup otherCg in World.CultureGroups.Where(ocg => ocg.Regions.Contains(randomAdjacency)))
                {
                    if (cultureGroupsToRemove.Contains(otherCg)) continue;
                    if (cultureGroupsThatOverlap.Contains(otherCg)) continue;
                    if (overlappedRegions.Contains(randomAdjacency)) continue;
                    cultureGroupsToRemove.Add(otherCg);
                    cultureGroupsThatOverlap.Add(cg);
                    cg.Regions.Add(randomAdjacency);
                    otherCg.Regions.Remove(randomAdjacency);
                    randomAdjacency.CultureGroup = cg;
                    overlappedRegions.Add(randomAdjacency);
                }
            }
            foreach (CultureGroup group in cultureGroupsToRemove)
            {
                World.CultureGroups.Remove(group);
            }
        }

        /// <summary>
        /// Generates a culture for each culture group for each area within the culture groups regions.
        /// </summary>
        private void GenerateCultures()
        {
            //Generates a culture for every area in each region and assigned to the region's culture group
            foreach (CultureGroup cultureGroup in World.CultureGroups)
            {
                //Get all areas in the culture group's regions
                List<Area> areas = (
                    from a in cultureGroup.Regions.SelectMany(cg => cg.Areas)
                    select a).ToList();

                foreach (Area area in areas)
                {
                    var culture = new Culture()
                    {
                        InternalName = NameGenerator.GetInternalCultureName(),
                        Area = area,
                        Type = cultureGroup.Type,
                        ParentCultureGroup = cultureGroup
                    };
                    area.Culture = culture;
                    cultureGroup.Cultures.Add(culture);
                }
            }
        }

        /// <summary>
        /// Randomly "Unifies" the culture group into one country
        /// </summary>
        private void DetermineCultureUnification()
        {
            //Only "advanced" cultures have a chance to unify
            foreach (CultureGroup cultureGroup in World.CultureGroups.Where(
                        c => c.TechGroup.Primitive == false && 
                        c.TechGroup.LimitedExpansion == false))
            {
                if (Utils.GetProbability(R.CultureUnificationChance))
                {
                    cultureGroup.Unified = true;
                }
            }
        }
    }
}
