﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class ReligionGenerator
    {
        private static ReligionGenerator _instance = new ReligionGenerator();

        private ReligionGenerator() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static ReligionGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the religion generator process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Generating religion groups...");
            GenerateReligionGroups();
            Console.WriteLine("Performing religion group overlap calculations...");
            ReligionGroupOverlap();
            Console.WriteLine("Generating religions...");
            GenerateReligions();
            ReligionOverlap();
            Console.WriteLine("Generating denominations...");
            GenerateDenominations();
            Console.WriteLine("Generating heretics...");
            GenerateHeretics();
            Console.WriteLine("Purging regionless religions...");
            PurgeRegionlessReligions();
            Console.WriteLine("Assigning religion icons...");
            AssignReligionIcons();
        }

        /// <summary>
        /// Names the religions based on the provinces encompassed by it.
        /// </summary>
        public void NameReligions()
        {
            foreach (var religionGroup in World.ReligionGroups)
            {
                var provincesInSuperRegions = (
                    from sr in religionGroup.SuperRegions
                    from r in sr.Regions
                    from a in r.Areas
                    from p in a.Provinces
                    select p).ToList();

                int randomSrProvinceIndex = R.Random.Next(0, provincesInSuperRegions.Count);
                var randomSrProvince = provincesInSuperRegions[randomSrProvinceIndex];

                string groupName = $"{randomSrProvince.Name}ism";
                religionGroup.Name = groupName;

                foreach (var religion in religionGroup.Religions)
                {
                    var provincesInRegions = (
                        from r in religion.Regions
                        from a in r.Areas
                        from p in a.Provinces
                        select p).ToList();

                    string religName;
                    if (provincesInRegions.Count > 0)
                    {
                        int randomRProvinceIndex = R.Random.Next(0, provincesInRegions.Count);
                        Province randomRProvince = provincesInRegions[randomRProvinceIndex];
                        religName = NameGenerator.GetDemonym(randomRProvince.Name);
                    }
                    else
                    {
                        throw new Exception("Regionless religion detected!");
                    }
                    religion.Name = religName;
                }
            }
        }

        /// <summary>
        /// Generates names for each heresy
        /// </summary>
        public void NameHeresies()
        {
            foreach (var religion in World.ReligionGroups.SelectMany(x => x.Religions))
            {
                var religionProvinces = (
                    from r in religion.Regions
                    from a in r.Areas
                    from p in a.Provinces
                    where p.Religion == religion
                    select p).ToList();

                foreach (var heresy in religion.Heresies)
                {
                    Province randomProvince = religionProvinces[R.Random.Next(0, religionProvinces.Count)];
                    heresy.Name = NameGenerator.GetDemonym(randomProvince.Name);
                }
            }
        }

        /// <summary>
        /// Generates a religion group for each Super Region
        /// </summary>
        private void GenerateReligionGroups()
        {
            //Generates a religion group for every SuperRegion
            foreach (SuperRegion superRegion in World.SuperRegions)
            {
                var religionGroup = new ReligionGroup();
                religionGroup.InternalName = NameGenerator.GetInternalReligionGroupName();
                religionGroup.SuperRegions.Add(superRegion);
                superRegion.ReligionGroup = religionGroup;
                World.ReligionGroups.Add(religionGroup);
            }
        }

        /// <summary>
        /// Randomly causes a religion group to overwrite/overlap another one
        /// </summary>
        private void ReligionGroupOverlap()
        {
            var religionGroupsToRemove = new List<ReligionGroup>();
            var religionGroupsThatOverlap = new List<ReligionGroup>();
            var overlappedSuperRegions = new List<SuperRegion>();

            foreach (ReligionGroup rg in World.ReligionGroups)
            {
                //If the religion is already slated for removal, skip
                if (religionGroupsToRemove.Contains(rg)) continue;

                //Gets all adjacencies assinged to the selected religion's superregion
                List<SuperRegion> srAdjacencies = (
                    from sr in rg.SuperRegions
                    from adj in sr.Adjacencies
                    select adj).ToList();

                if (!Utils.GetProbability(R.ReligionGroupOverlapChance)) continue;

                //Gets a random index within the bounds of the adjacency list
                int randomAdjacencyIndex = R.Random.Next(0, srAdjacencies.Count);

                //Gets the adjacency given the random index
                SuperRegion randomAdjacency = srAdjacencies[randomAdjacencyIndex];

                foreach (ReligionGroup otherRg in World.ReligionGroups.Where(org => org.SuperRegions.Contains(randomAdjacency)))
                {
                    //If the other religion group has already overlapped another religion group, don't run calculations
                    if (religionGroupsThatOverlap.Contains(otherRg)) continue;
                    //If the other super region has already been overlapped
                    if (overlappedSuperRegions.Contains(randomAdjacency)) continue;
                    //Add the other religion group to the remove list
                    religionGroupsToRemove.Add(otherRg);
                    //Add the religion group to the overlap list
                    religionGroupsThatOverlap.Add(rg);
                    //Add the super region adjacency that once belonged to the other religion group to the religion group
                    rg.SuperRegions.Add(randomAdjacency);
                    //Assigns the new rg to all regions that had the old rg
                    foreach (var region in World.SuperRegions.Where(x => x.ReligionGroup == otherRg))
                    {
                        region.ReligionGroup = rg;
                    }
                    //Add the super reigon adjacecny to the overlapped list
                    overlappedSuperRegions.Add(randomAdjacency);
                }
            }
            //Remove all religion groups slated for removal
            foreach (ReligionGroup group in religionGroupsToRemove)
            {
                World.ReligionGroups.Remove(group);
            }
        }

        /// <summary>
        /// Creates a primary religion for each SuperRegion.
        /// </summary>
        private void GenerateReligions()
        {
            foreach (ReligionGroup religionGroup in World.ReligionGroups)
            {
                Religion religion = CreateReligionFromGroup(religionGroup);
                foreach (Region region in religionGroup.SuperRegions.SelectMany(r => r.Regions))
                {
                    region.Religion = religion;
                    religion.Regions.Add(region);
                }
            }
        }

        /// <summary>
        /// Creates a new religion from a group
        /// </summary>
        /// <param name="religionGroup">The group to make a religion out of</param>
        /// <returns>The newly created religion</returns>
        private Religion CreateReligionFromGroup(ReligionGroup religionGroup)
        {
            var religion = new Religion();
            religion.InternalName = NameGenerator.GetInternalReligionName();
            religion.SetRgb(GetRandomReligionRgb());
            religion.ParentReligionGroup = religionGroup;
            religionGroup.Religions.Add(religion);
            return religion;
        }

        /// <summary>
        /// Generated denominations for the world
        /// </summary>
        private void GenerateDenominations()
        {
            //Chance of new denomination in religion groups that 
            foreach (var sr in World.ReligionGroups.Where(x => x.SuperRegions.Count > 1).SelectMany(y => y.SuperRegions))
            {
                if (!Utils.GetProbability(R.SuperRegionNewDenominationChance)) continue;

                //Create Denomination encompassing entire superregion
                Religion religion = CreateReligionFromGroup(sr.ReligionGroup);

                foreach (var region in sr.Regions)
                {
                    //Remove the region from the old religion
                    region.Religion.Regions.Remove(region);
                    //Add the region to the new religion
                    religion.Regions.Add(region);
                    //Assign the new religion to the region
                    region.Religion = religion;
                }
            }
            foreach (var religionGroup in World.ReligionGroups)
            {
                foreach (var region in religionGroup.SuperRegions.SelectMany(x => x.Regions))
                {
                    if (!Utils.GetProbability(R.RegionNewDenominationChance)) continue;

                    //Create Denomination encompassing entire region
                    Religion religion = CreateReligionFromGroup(region.ParentSuperRegion.ReligionGroup);

                    region.Religion.Regions.Remove(region);
                    religion.Regions.Add(region);
                    region.Religion = religion;
                }
            }
        }

        /// <summary>
        /// Generates up to 5 random heretic names for each religion
        /// </summary>
        private void GenerateHeretics()
        {
            foreach (var religion in World.ReligionGroups.SelectMany(x => x.Religions))
            {
                for (int i = 0; i < R.Random.Next(1, R.MaxHereticsPerReligion + 1); i++)
                {
                    religion.Heresies.Add(new Heresy() {
                        InternalName = NameGenerator.GetInternalHereticName()
                    });
                }
            }
        }

        /// <summary>
        /// Randomly makes one religion overlap another adjacent religion
        /// </summary>
        private void ReligionOverlap()
        {
            var overlappedRegions = new List<Region>();

            foreach (Religion religion in World.ReligionGroups.SelectMany(r => r.Religions))
            {
                if(!Utils.GetProbability(R.ReligionOverlapChance)) continue;

                List<Region> adjcacencies = (
                    from r in religion.Regions
                    from adj in r.Adjacencies
                    select adj).Distinct().ToList();

                List<Region> differentReligionRegions = adjcacencies.Where
                    (adjcacency => adjcacency.Religion != religion).ToList();

                if(differentReligionRegions.Count == 0) continue;

                int randomAdjacencyIndex = R.Random.Next(0, differentReligionRegions.Count);
                Region randomAdjacency = differentReligionRegions[randomAdjacencyIndex];

                if(overlappedRegions.Contains(randomAdjacency)) continue;

                randomAdjacency.Religion = religion;
                randomAdjacency.Religion.Regions.Remove(randomAdjacency);
                religion.Regions.Add(randomAdjacency);
                overlappedRegions.Add(randomAdjacency);
            }
        }

        /// <summary>
        /// Removes regionless religions from the global religions list
        /// </summary>
        private void PurgeRegionlessReligions()
        {
            var regionlessReligions = (
                from rg in World.ReligionGroups
                from r in rg.Religions
                where r.Regions.Count == 0
                select r).ToList();

            if (regionlessReligions.Count() != 0)
            {
                //Gets all religion groups that contain a regionless religion
                foreach (var religionGroup in World.ReligionGroups.Where(rg => rg.Religions.Any(r => regionlessReligions.Contains(r))))
                {
                    var matches = (
                        from r in religionGroup.Religions
                        where regionlessReligions.Contains(r)
                        select r).ToArray();

                    if (!matches.Any()) continue;

                    foreach (var match in matches)
                    {
                        religionGroup.Religions.Remove(match);
                    }
                }
            }
        }

        /// <summary>
        /// Assigns religion icons to every religion. Tries to be unique
        /// </summary>
        private void AssignReligionIcons()
        {
            var usedIconSets = new List<ReligionIcon>();

            foreach (var religionGroup in World.ReligionGroups)
            {
                var unusedIconSets = World.ReligionIcons.Where(x => !usedIconSets.Contains(x)).ToList();

                //Tries to get a unique random icon set
                ReligionIcon randomIconSet;
                if (unusedIconSets.Count > 0)
                {
                    //Normal Assignment
                    var randomIconSetIndex = R.Random.Next(0, unusedIconSets.Count);
                    randomIconSet = unusedIconSets[randomIconSetIndex];
                    usedIconSets.Add(randomIconSet);
                }
                else
                {
                    //Assignment disregarding used icon sets
                    var randomIconSetIndex = R.Random.Next(0, World.ReligionIcons.Count);
                    randomIconSet = World.ReligionIcons[randomIconSetIndex];
                }

                //Tries to get a unique random icon from the icon set
                List<int> usedIcons = new List<int>();
                foreach (var religion in religionGroup.Religions)
                {
                    var unusedIcons = randomIconSet.Icons.Where(x => !usedIcons.Contains(x)).ToList();

                    int randomIcon;
                    if (unusedIcons.Count > 0)
                    {
                        //Normal Assignment
                        var randomIconIndex = R.Random.Next(0, unusedIcons.Count);
                        randomIcon = unusedIcons[randomIconIndex];
                        usedIcons.Add(randomIcon);
                    }
                    else
                    {
                        //Assignment disregarding used icons
                        var randomIconIndex = R.Random.Next(0, randomIconSet.Icons.Count);
                        randomIcon = randomIconSet.Icons[randomIconIndex];
                    }
                    religion.Icon = randomIcon;
                }
            }
        }

        /// <summary>
        /// Gets a random RGB value formatted for a religion
        /// </summary>
        /// <returns>The RGB as a string array</returns>
        private string[] GetRandomReligionRgb()
        {
            int r1 = R.Random.Next(0, 9);
            int r2 = R.Random.Next(0, 9);
            string red = $"0.{r1}{r2}";

            int g1 = R.Random.Next(0, 9);
            int g2 = R.Random.Next(0, 9);
            string green = $"0.{g1}{g2}";

            int b1 = R.Random.Next(0, 9);
            int b2 = R.Random.Next(0, 9);
            string blue = $"0.{b1}{b2}";

            string[] rgb = { red, green, blue };
            return rgb;
        }
    }
}
