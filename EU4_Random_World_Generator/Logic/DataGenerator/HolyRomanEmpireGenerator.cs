﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class HolyRomanEmpireGenerator
    {
        private static HolyRomanEmpireGenerator _instance = new HolyRomanEmpireGenerator();

        private HolyRomanEmpireGenerator() { }

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static HolyRomanEmpireGenerator Instance()
        {
            return _instance;
        }

        /// <summary>
        /// Runs the Holy Roman Empire Generation process
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Assigning HRE to culture group...");
            SetHolyRomanEmpire();
            Console.WriteLine("Setting HRE emperor and electors...");
            SetHreEmperorAndElectors();
            Console.WriteLine("Setting HRE free cities...");
            SetHreFreeCities();
        }

        private List<Country> _hreCountries;

        /// <summary>
        /// Assigns the culture group with the large number of countries as the Holy Roman Empire
        /// </summary>
        private void SetHolyRomanEmpire()
        {
            var validHreCultureGroups = (
                from cg in World.CultureGroups
                where !cg.Unified
                where !cg.TechGroup.LimitedExpansion
                where !cg.TechGroup.Primitive
                where !cg.TechGroup.CanMigrate
                where !cg.TechGroup.HasTribalNonMigratoryGovernments
                where !cg.TechGroup.SteppeNomad
                where cg.Regions.Count > 0
                select cg).ToList();

            //Gets every country for each valid HRE culture group
            List<List<Country>> validHreCountriesPerCultureGroup = new List<List<Country>>();
            foreach (var cultureGroup in validHreCultureGroups)
            {
                List<Country> countriesinCultureGroup = (
                    from c in World.Countries
                    where cultureGroup.Equals(c.PrimaryCulture.ParentCultureGroup)
                    select c).ToList();
                validHreCountriesPerCultureGroup.Add(countriesinCultureGroup);
            }

            //Sorts the list by number of countries and assignes the largest to the hreCountries list
            validHreCountriesPerCultureGroup.Sort((a,b) => b.Count - a.Count);
            _hreCountries = validHreCountriesPerCultureGroup.First();

            //Might be unnecessary
            _hreCountries[0].PrimaryCulture.ParentCultureGroup.HolyRomanEmpire = true;

            foreach (var country in _hreCountries)
            {
                country.InHre = true;
                country.CapitalProvince.InHre = true;
            }
        }

        /// <summary>
        /// Assigns the role of Emperor and Electors to random countries in the HRE
        /// </summary>
        private void SetHreEmperorAndElectors()
        {
            if (_hreCountries.Count == 0)
            {
                throw new Exception("HRE is empty!");
            }

            //Emperor is set and removed from hreCountries List
            int emperorIndex = R.Random.Next(0, _hreCountries.Count);
            Country emperor = _hreCountries[emperorIndex];
            emperor.IsHreEmperor = true;
            _hreCountries.Remove(emperor);

            //Princes are determined from the remaining countries
            int princeCount = 7;
            List<Country> princes = new List<Country>();
            if (_hreCountries.Count < 7)
            {
                princeCount = _hreCountries.Count;
            }
            for (int i = 0; i < princeCount; i++)
            {
                int randomHreIndex = R.Random.Next(0, princeCount);
                Country prince = _hreCountries[randomHreIndex];

                while (princes.Contains(prince))
                {
                    randomHreIndex = R.Random.Next(0, princeCount);
                    prince = _hreCountries[randomHreIndex];
                }
                prince.IsHreElector = true;
                princes.Add(prince);
            }

            //Sets every HRE country's religion to the emperor's
            foreach (var country in _hreCountries)
            {
                country.Religion = emperor.Religion;
                country.CapitalProvince.Religion = emperor.Religion;
            }
        }

        /// <summary>
        /// Assigns free city designation to random HRE countries that meet certain conditions
        /// </summary>
        private void SetHreFreeCities()
        {
            var freeCityGovernment = World.Governments.FirstOrDefault(g => g.FreeCity);

            //Skip free city generation if free city government doesn't exist
            if (freeCityGovernment == null) return;

            var freeCityCandidates = _hreCountries
                .Where(x => !x.IsHreElector)
                .Where(x => x.CapitalProvince.Adm +
                            x.CapitalProvince.Dip +
                            x.CapitalProvince.Mil > 10).ToList();

            List<Country> freeCities = new List<Country>();

            if (freeCityCandidates.Count >= 8)
            {
                for (int i = 0; i < 8; i++)
                {
                    int randomCountryIndex = R.Random.Next(0, freeCityCandidates.Count);
                    Country freeCity = freeCityCandidates[randomCountryIndex];

                    while (freeCities.Contains(freeCity))
                    {
                        randomCountryIndex = R.Random.Next(0, freeCityCandidates.Count);
                        freeCity = freeCityCandidates[randomCountryIndex];
                        freeCity.Government = freeCityGovernment;
                    }

                    freeCity.IsHreFreeCity = true;
                    freeCities.Add(freeCity);
                }
            }
            else
            {
                foreach (Country freeCity in freeCityCandidates)
                {
                    freeCity.IsHreFreeCity = true;
                    freeCity.Government = freeCityGovernment;
                }
            }
        }
    }
}
