﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace EU4_Random_World_Generator
{
    class NameGenerator
    {
        public static List<string> GeneratedNames = new List<string>();
        public static List<string> GeneratedCultureNames = new List<string>();

        /// <summary>
        /// Gets a random culture type
        /// </summary>
        /// <returns>A random culture type</returns>
        public static CultureType GetRandomCultureType()
        {
            int index = R.Random.Next(0, World.CultureTypes.Count);
            return World.CultureTypes[index];
        }

        /// <summary>
        /// Generates a random person's name given a culture type and gender
        /// </summary>
        /// <param name="type">The culture type</param>
        /// <param name="gender">The gender</param>
        /// <returns>The name as a string</returns>
        public static string GeneratePersonName(CultureType type, string gender)
        {
            if (type == null)
            {
                throw new Exception("Culture type does not exist!");
            }

            string generatedName = "";

            while (GeneratedNames.Contains(generatedName) || generatedName == "")
            {
                generatedName = "";

                if (gender == "Male")
                {
                    //Prefix
                    if (Utils.GetProbability(R.MalePrefixChance) && type.MaleNameSyllablesPrefix.Count > 0)
                    {
                        int index = R.Random.Next(0, type.MaleNameSyllablesPrefix.Count);
                        generatedName += type.MaleNameSyllablesPrefix[index];
                    }

                    //First
                    int firstIndex = R.Random.Next(0, type.MaleNameSyllablesFirst.Count);
                    generatedName += type.MaleNameSyllablesFirst[firstIndex];

                    //Middle
                    if (Utils.GetProbability(R.MaleMiddleSyllableChance) && type.MaleNameSyllablesMiddle.Count > 0)
                    {
                        int index = R.Random.Next(0, type.MaleNameSyllablesMiddle.Count);
                        generatedName += type.MaleNameSyllablesMiddle[index];
                    }

                    //Last
                    int lastIndex = R.Random.Next(0, type.MaleNameSyllablesLast.Count);
                    generatedName += type.MaleNameSyllablesLast[lastIndex];
                }
                else //Female
                {
                    //Prefix
                    if (Utils.GetProbability(R.FemalePrefixChance) && type.FemaleNameSyllablesPrefix.Count > 0)
                    {
                        int index = R.Random.Next(0, type.FemaleNameSyllablesPrefix.Count);
                        generatedName += type.FemaleNameSyllablesPrefix[index];
                    }

                    //First
                    int firstIndex = R.Random.Next(0, type.FemaleNameSyllablesFirst.Count);
                    generatedName += type.FemaleNameSyllablesFirst[firstIndex];

                    //Middle
                    if (Utils.GetProbability(R.FemaleMiddleSyllableChance) && type.FemaleNameSyllablesMiddle.Count > 0)
                    {
                        int index = R.Random.Next(0, type.FemaleNameSyllablesMiddle.Count);
                        generatedName += type.FemaleNameSyllablesMiddle[index];
                    }

                    //Last
                    int lastIndex = R.Random.Next(0, type.FemaleNameSyllablesLast.Count);
                    generatedName += type.FemaleNameSyllablesLast[lastIndex];
                }
            }
            GeneratedNames.Add(generatedName);
            return generatedName;
        }

        /// <summary>
        /// Gets a random place name given the type
        /// </summary>
        /// <param name="type">The culture type</param>
        /// <param name="noDupes">Whether the generation should create dupes or not</param>
        /// <returns>The place name as a string</returns>
        public static string GeneratePlaceName(CultureType type, bool noDupes = false)
        {
            if (type == null)
            {
                throw new Exception("Culture type does not exist!");
            }

            string generatedName = "";

            while (GeneratedNames.Contains(generatedName) || generatedName == "")
            {
                generatedName = "";

                //Prefix
                if (Utils.GetProbability(R.PlacePrefixChance))
                {
                    int index = R.Random.Next(0, type.PlaceSyllablesPrefix.Count);
                    generatedName += type.PlaceSyllablesPrefix[index];
                }

                //First
                int firstIndex = R.Random.Next(0, type.PlaceSyllablesFirst.Count);
                generatedName += type.PlaceSyllablesFirst[firstIndex];

                //Middle
                if (Utils.GetProbability(R.PlaceMiddleSyllableChance) && type.PlaceSyllablesMiddle.Count > 0)
                {
                    int index = R.Random.Next(0, type.PlaceSyllablesMiddle.Count);
                    generatedName += type.PlaceSyllablesMiddle[index];
                }

                //Last
                int lastIndex = R.Random.Next(0, type.PlaceSyllablesLast.Count);
                generatedName += type.PlaceSyllablesLast[lastIndex];
            }
            if (noDupes)
            {
                GeneratedNames.Add(generatedName);
            }
            return generatedName;
        }

        /// <summary>
        /// Generates a culture name given a culture type
        /// </summary>
        /// <param name="type">The culture type</param>
        /// <returns>Random culture name</returns>
        public static string GenerateCultureName(CultureType type)
        {
            string cultureName = "";
            while(GeneratedCultureNames.Contains(cultureName) || cultureName == "")
            {
                cultureName = GetDemonym(GeneratePlaceName(type));
            }
            GeneratedCultureNames.Add(cultureName);
            return cultureName;
        }

        /// <summary>
        /// Gets the demonym of a given name
        /// </summary>
        /// <param name="cultureName">The name of the culture</param>
        /// <returns>The demonym of the given name</returns>
        public static string GetDemonym(string cultureName)
        {
            int startIndex = cultureName.Length - 2;
            int endIndex = cultureName.Length - startIndex;
            string cultureEnding = cultureName.Substring(startIndex, endIndex);
            string suffix;

            List<string> suffixes = (
                from suff in World.SuffixPairs
                where suff.Ending == cultureEnding
                select suff.Suffix).ToList();

            if(suffixes.Count < 1)
            {
                Debug.WriteLine($"WARNING: No suffix found for ending \"{cultureEnding}\" using \"an\"");
                suffix = "an";
            }
            else
            {
                int randomSuffix = R.Random.Next(0, suffixes.Count);
                suffix = suffixes[randomSuffix];
            }

            return cultureName + suffix;
        }

        /// <summary>
        /// Sanitizes the input for internal use
        /// </summary>
        /// <param name="name">The name to sanitize</param>
        /// <returns>The sanitized name</returns>
        public static string GetSanitizedName(string name)
        {
            Regex rgx = new Regex("[^A-Za-z0-9]");
            return rgx.Replace(name, "X");
        }

        /*
         The following are methods that individually count up
         internal name counters. These could maybe be lumped
         into a single method with a switch, but whatever
        */
        private static int _cultureGroupCount;
        public static string GetInternalCultureGroupName()
        {
            string internalName =  $"culturegroup_{_cultureGroupCount:000000}";
            _cultureGroupCount++;
            return internalName;
        }

        private static int _cultureCount;
        public static string GetInternalCultureName()
        {
            string internalName = $"culture_{_cultureCount:000000}";
            _cultureCount++;
            return internalName;
        }

        private static int _religionGroupCount;
        public static string GetInternalReligionGroupName()
        {
            string internalName = $"religiongroup_{_religionGroupCount:000000}";
            _religionGroupCount++;
            return internalName;
        }

        private static int _religionCount;
        public static string GetInternalReligionName()
        {
            string internalName = $"religion_{_religionCount:000000}";
            _religionCount++;
            return internalName;
        }

        private static int _hereticCount;
        public static string GetInternalHereticName()
        {
            string internalName = $"heretic_{_hereticCount:000000}";
            _hereticCount++;
            return internalName;
        }
    }
}
