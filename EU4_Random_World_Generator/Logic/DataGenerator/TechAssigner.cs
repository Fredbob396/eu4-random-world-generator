﻿using System.Collections.Generic;
using System.Linq;

namespace EU4_Random_World_Generator
{
    class TechAssigner
    {
        private static TechAssigner _instance = new TechAssigner();

        private TechAssigner() {}

        /// <summary>
        /// Gets the Instance
        /// </summary>
        /// <returns>The Instance</returns>
        public static TechAssigner Instance()
        {
            return _instance;
        }

        private readonly List<Tech> _usedTechGroups = new List<Tech>();

        /// <summary>
        /// Runs the tech assignment process
        /// </summary>
        public void Run()
        {
            AssignTechGroups();
        }

        private void AssignTechGroups()
        {
            foreach (SuperRegion sr in World.SuperRegions)
            {
                var unusedTech = (
                    from tg in World.TechGroups
                    where !_usedTechGroups.Contains(tg)
                    select tg).ToList();

                if (unusedTech.Count > 0)
                {
                    //Gets a random tech from the unused tech list
                    int techIndex = R.Random.Next(0, unusedTech.Count);
                    Tech tech = unusedTech[techIndex];
                    sr.TechGroup = tech;
                    _usedTechGroups.Add(tech);
                }
                else
                {
                    //Gets a random tech from the global tech list
                    int techIndex = R.Random.Next(0, World.TechGroups.Count);
                    Tech tech = World.TechGroups[techIndex];
                    sr.TechGroup = tech;
                }
            }
        }
    }
}
