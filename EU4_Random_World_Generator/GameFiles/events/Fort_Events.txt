namespace = fort_thing

country_event = {
	id = fort_thing.1
	title = "fort_thingEVTNAME1"
	desc = "fort_thingEVTDESC1"
	picture = COLONIZATION_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_country_flag = forts_placed }
	}
	
	option = {
		name = "fort_thingEVTOPTA1"
		FROM = { country_event = { id = fort_thing.2 } }
	}
}

country_event = {
	id = fort_thing.2
	title = "fort_thingEVTNAME2"
	desc = "fort_thingEVTDESC2"
	picture = COLONIZATION_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = "fort_thingEVTOPTA2"
		set_country_flag = forts_placed
		random_owned_province = {
			limit = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
				NOT = { has_building = fort_15th }
				NOT = { is_capital = yes }
			}
			province_event = { id = fort_thing.3 }
		}
		
		random_owned_province = {
			limit = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
				NOT = { has_building = fort_15th }
				NOT = { is_capital = yes }
			}
			province_event = { id = fort_thing.3 }
		}
		
		random_owned_province = {
			limit = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
				NOT = { has_building = fort_15th }
				NOT = { is_capital = yes }
			}
			province_event = { id = fort_thing.3 }
		}
	}
}

province_event = {
	id = fort_thing.3
	title = "fort_thingEVTNAME3"
	desc = "fort_thingEVTDESC3"
	picture = COLONIZATION_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = "fort_thingEVTOPTA3"
		add_building = fort_15th
	}
}