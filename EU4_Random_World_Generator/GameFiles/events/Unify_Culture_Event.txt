namespace = unify_culture

#Event that triggers for the annexees
country_event = {
	id = unify_culture.1
	title = "unify_cultureEVTNAME1"
	desc = "unify_cultureEVTDESC1"
	picture = COLONIZATION_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = "unify_cultureEVTOPTA1"
		every_owned_province = {
			cede_province = FROM
			add_core = FROM
		}
	}
}

#Event triggered for all unification leaders after everything is done expanding
country_event = {
	id = unify_culture.2
	title = "unify_cultureEVTNAME2"
	desc = "unify_cultureEVTDESC2"
	picture = COLONIZATION_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = unification_leader
		NOT = { has_country_flag = done_unifying }
	}
	
	option = {
		name = "unify_cultureEVTOPTA2"
		every_country = {
			limit = {
				culture_group_claim = ROOT
				has_country_flag = unification_subject
			}
			country_event = { id = unify_culture.1 }
		}
		set_country_flag = done_unifying
		add_manpower = 20
		add_sailors = 2
		add_treasury = 250
	}
}