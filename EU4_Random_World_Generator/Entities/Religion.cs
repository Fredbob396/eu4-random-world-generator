﻿using System;
using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class Religion
    {
        public string Name { get; set; }
        public string InternalName { get; set; }
        public List<Heresy> Heresies = new List<Heresy>();
        public int Icon { get; set; }
        public List<Region> Regions = new List<Region>();
        public ReligionGroup ParentReligionGroup { get; set; }

        private string _red;
        private string _green;
        private string _blue;

        public void SetRgb(string[] rgb)
        {
            if (rgb.Length == 3)
            {
                _red = rgb[0];
                _green = rgb[1];
                _blue = rgb[2];
            }
            else
            {
                throw new Exception("RGB array must contain 3 elements. No more, no less.");
            }
        }

        public string GetRgb()
        {
            string rgb = $" {_red} {_green} {_blue} ";
            return rgb;
        }
    }
}
