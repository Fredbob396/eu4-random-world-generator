﻿namespace EU4_Random_World_Generator
{
    class Country
    {
        public string Name { get; set; }
        public string Demonym { get; set; }
        public string Tag { get; set; }
        public Culture PrimaryCulture { get; set; } //primary_culture = francien
        public Religion Religion { get; set; } //religion = catholic
        public Government Government { get; set; } //government = feudal_monarchy
        public int GovernmentRank { get; set; } //government_rank = 2
        public Tech TechGroup { get; set; } //technology_group = western
        public Province CapitalProvince = new Province();
        public bool IsEmpire { get; set; }
        public bool InHre { get; set; }
        public bool IsHreEmperor { get; set; }
        public bool IsHreElector { get; set; }
        public bool IsHreFreeCity { get; set; }
        public bool UnifiedCultureLeader { get; set; }
        public bool UnifiedCultureSubject { get; set; }
        public int[] Rgb { get; set; } = {0, 0, 0};

        public string GetRgb()
        {
            string rgb = $"{Rgb[0]} {Rgb[1]} {Rgb[2]}";
            return rgb;
        }
    }
}
