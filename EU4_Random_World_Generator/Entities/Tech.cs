﻿using EU4FlagLib;

namespace EU4_Random_World_Generator
{
    public class Tech
    {
        public string Name { get; set; }
        public string Gfx { get; set; }
        public bool Primitive { get; set; }
        public bool CanMigrate { get; set; }
        public bool SteppeNomad { get; set; }
        public bool LimitedExpansion { get; set; }
        public bool HasTribalNonMigratoryGovernments { get; set; }
        public FlagComponents FlagComponents { get; set; }
    }
}
