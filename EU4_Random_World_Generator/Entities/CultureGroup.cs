﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class CultureGroup
    {
        public string Name { get; set; }
        public string UnDemonynmizedName { get; set; }
        public string InternalName { get; set; }
        public List<Region> Regions = new List<Region>(); //Names of regions this Culture Group is associated with
        public CultureType Type { get; set; } //This determines how it's sub-cultures are generated
        public Tech TechGroup { get; set; }
        public List<Culture> Cultures = new List<Culture>();
        public List<string> MaleNames = new List<string>();
        public List<string> FemaleNames = new List<string>();
        public List<string> Surnames = new List<string>();
        public bool Unified { get; set; }
        public bool Empire { get; set; }
        public bool HolyRomanEmpire { get; set; }
    }
}
