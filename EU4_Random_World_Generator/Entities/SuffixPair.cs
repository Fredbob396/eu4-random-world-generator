﻿namespace EU4_Random_World_Generator
{
    public struct SuffixPair
    {
        public string Ending { get; set; }
        public string Suffix { get; set; }
    }
}
