﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class CultureType
    {
        public string Name { get; set; }
        public List<string> PlaceSyllablesPrefix = new List<string>();
        public List<string> PlaceSyllablesFirst = new List<string>();
        public List<string> PlaceSyllablesMiddle = new List<string>();
        public List<string> PlaceSyllablesLast = new List<string>();

        public List<string> MaleNameSyllablesPrefix = new List<string>();
        public List<string> MaleNameSyllablesFirst = new List<string>();
        public List<string> MaleNameSyllablesMiddle = new List<string>();
        public List<string> MaleNameSyllablesLast = new List<string>();

        public List<string> FemaleNameSyllablesPrefix = new List<string>();
        public List<string> FemaleNameSyllablesFirst = new List<string>();
        public List<string> FemaleNameSyllablesMiddle = new List<string>();
        public List<string> FemaleNameSyllablesLast = new List<string>();
    }
}
