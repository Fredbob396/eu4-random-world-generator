﻿using System;
using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class Province
    {
        public string Name { get; set; }
        public string Demonym { get; set; }
        public Country Owner { get; set; }
        //public string Owner { get; set; }
        public Culture Culture { get; set; }
        public Religion Religion { get; set; }
        public int Id { get; set; }
        public int Adm { get; set; }
        public int Dip { get; set; }
        public int Mil { get; set; }
        public string TradeGoods { get; set; }
        public List<string> DiscoveredBy = new List<string>();
        public bool IsCity { get; set; }
        public bool HasFort { get; set; }
        public bool InHre { get; set; }
        public Area ParentArea { get; set; }
        public Region ParentRegion { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }

        public void SetDevelopment(int[] development)
        {
            if (development.Length == 3)
            {
                Adm = development[0];
                Dip = development[1];
                Mil = development[2];
            }
            else
            {
                throw new Exception("Development array must contain 3 elements. No more, no less.");
            }
        }
    }
}
