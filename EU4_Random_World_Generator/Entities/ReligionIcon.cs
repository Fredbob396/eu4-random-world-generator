﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class ReligionIcon
    {
        public string Category;
        public List<int> Icons = new List<int>();
    }
}