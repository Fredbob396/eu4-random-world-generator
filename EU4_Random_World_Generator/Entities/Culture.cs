﻿namespace EU4_Random_World_Generator
{
    class Culture
    {
        public string Name { get; set; }
        public string UnDemonymizedName { get; set; }
        public string InternalName { get; set; }
        public Area Area { get; set; }
        public CultureType Type { get; set; }
        public CultureGroup ParentCultureGroup { get; set; }
    }
}
