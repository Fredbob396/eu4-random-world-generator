﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class SuperRegion
    {
        public string Name { get; set; }
        public List<Region> Regions = new List<Region>();
        public List<string> RegionNames = new List<string>();
        public List<SuperRegion> Adjacencies = new List<SuperRegion>();
        public ReligionGroup ReligionGroup { get; set; }
        public Tech TechGroup { get; set; }
        public bool IsSteppe { get; set; } //Steppe hordes
        public bool IsTribal { get; set; } //Native Americans
        public bool IsPartiallySettled { get; set; } //Mesoamerican Equivalent. Expands very little on intialization.
    }
}
