﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class Region
    {
        public string Name { get; set; }
        public List<Area> Areas = new List<Area>();
        public List<string> AreaNames = new List<string>();
        public List<Region> Adjacencies = new List<Region>();
        public CultureGroup CultureGroup { get; set; }
        public Religion Religion { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }
    }
}
