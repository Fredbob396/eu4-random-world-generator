﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class Area
    {
        public string Name { get; set; }
        public List<Province> Provinces = new List<Province>();
        public List<string> ProvinceIds = new List<string>();
        public Region ParentRegion { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }
        public Culture Culture { get; set; }
    }
}
