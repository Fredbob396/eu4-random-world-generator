﻿namespace EU4_Random_World_Generator
{
    class Government
    {
        public string Name { get; set; }
        public int MinRank { get; set; }
        public int MaxRank { get; set; }
        public bool Republic { get; set; }
        public bool Theocracy { get; set; }
        public bool MigratingTribal { get; set; }
        public bool SteppeNomad { get; set; }
        public bool NonMigratingTribal { get; set; }
        public bool Rare { get; set; }
        public bool VeryRare { get; set; }
        public bool FreeCity { get; set; }
    }
}
