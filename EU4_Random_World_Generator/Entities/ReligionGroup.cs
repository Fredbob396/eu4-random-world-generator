﻿using System.Collections.Generic;

namespace EU4_Random_World_Generator
{
    class ReligionGroup
    {
        public string Name { get; set; }
        public string InternalName { get; set; }
        public List<Religion> Religions = new List<Religion>();
        public List<SuperRegion> SuperRegions = new List<SuperRegion>(); //Names of SuperRegions this Religion Group is associated with
    }
}
