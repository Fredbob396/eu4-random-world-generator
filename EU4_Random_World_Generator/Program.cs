﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;

namespace EU4_Random_World_Generator
{
    class Program
    {
        /// <summary>
        /// The entry point of the program.
        /// </summary>
        /// <param name="args">The seed</param>
        public static void Main(string[] args)
        {
            string arg = ParseArguments(args);

            var p = new Program(arg);

            #if(DEBUG)
                p.RunDebug();
            #else
                p.Run();
            #endif
        }

        /// <summary>
        /// Parses the arguments. If none are found, a random seed is returned
        /// </summary>
        /// <param name="args">The arguments</param>
        private static string ParseArguments(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("No seed found! Generating random seed...");
                string randomSeed = GetRandomSeed();
                Console.WriteLine($"Random Seed: {randomSeed}");
                return randomSeed;
            }
            return args.Length > 1
                ? string.Join(" ", args)
                : args[0];
        }

        /// <summary>
        /// Generates a random seed
        /// </summary>
        /// <returns>A random seed</returns>
        private static string GetRandomSeed()
        {
            var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] seedBytes = new byte[16];
            rngCryptoServiceProvider.GetBytes(seedBytes);

            string randomSeed = BitConverter.ToString(seedBytes).Replace("-", "");
            return randomSeed;
        }

        /// <summary>
        /// Initializes the program with a seed passed in from the main
        /// </summary>
        /// <param name="seed"></param>
        private Program(string seed)
        {
            R.Seed = seed;
            R.Random = new Random(seed.GetHashCode());
        }

        /// <summary>
        /// Runs the program
        /// </summary>
        private void Run()
        {
            Console.WriteLine("### MAP LOADER ###");
            var mapLoader = MapLoader.Instance();
            mapLoader.Run();

            Console.WriteLine("### DATABASE LOADER ###");
            var databaseLoader = DatabaseLoader.Instance();
            databaseLoader.Run();

            Console.WriteLine("### FLAGS LOADER ###");
            var flagsLoader = FlagsLoader.Instance();
            flagsLoader.Run();

            Console.WriteLine("###JSON LOADER ###");
            var jsonLoader = JsonLoader.Instance();
            jsonLoader.Run();

            Console.WriteLine("### DATA GENERATION ###");
            var dataGenerator = DataGenerator.Instance();
            dataGenerator.Run();

            Console.WriteLine("### FILE GENERATION ###");
            var fileGenerator = FileGenerator.Instance();
            fileGenerator.Run();

            Console.WriteLine("Done! Press any key to quit");
            Console.ReadKey(true);
        }

        /// <summary>
        /// Runs the program in debug mode
        /// </summary>
        private void RunDebug()
        {
            Console.WriteLine(">>>DEBUG MODE ACTIVE<<<");

            Console.WriteLine("### MAP LOADER ###");
            Stopwatch debugWatch = Stopwatch.StartNew();
            var mapLoader = MapLoader.Instance();
            mapLoader.Run();
            debugWatch.Stop();
            Console.WriteLine($"Map File Load Time: {debugWatch.ElapsedMilliseconds}ms");

            Console.WriteLine("### DATABASE LOADER ###");
            debugWatch = Stopwatch.StartNew();
            var databaseLoader = DatabaseLoader.Instance();
            databaseLoader.Run();
            debugWatch.Stop();
            Console.WriteLine($"DB Load Time: {debugWatch.ElapsedMilliseconds}ms");

            Console.WriteLine("### FLAGS LOADER ###");
            var flagsLoader = FlagsLoader.Instance();
            flagsLoader.Run();

            Console.WriteLine("###JSON LOADER ###");
            var jsonLoader = JsonLoader.Instance();
            jsonLoader.Run();

            #region Post-Load Test Variables
            var techGroupsTest = World.TechGroups;
            var governmentsTest = World.Governments;
            var cultureTypesTest = World.CultureTypes;
            var testSuperRegions = World.SuperRegions;
            var religionIconsTest = World.ReligionIcons;
            #endregion

            Console.WriteLine("### DATA GENERATION ###");
            var dataGenerator = DataGenerator.Instance();
            dataGenerator.Run();

            #region Post-Gen Test Variables
            var hreCountries = (
                from c in World.Countries
                where c.InHre
                select c).ToList();

            var hreElectors = (
                from c in World.Countries
                where c.IsHreElector
                select c).ToList();

            var hreEmperor = (
                from c in World.Countries
                where c.IsHreEmperor
                select c).First();

            var hreFreeCities = (
                from c in World.Countries
                where c.Government.FreeCity
                select c).ToList();

            var testReligions = World.ReligionGroups;
            var testCultures = World.CultureGroups;
            var testCountries = World.Countries;

            List<Country> testUnificationLeaders = (
                from c in World.Countries
                where c.UnifiedCultureLeader
                select c).ToList();

            var testMultiRegionCultureGroups = (
                from c in World.CultureGroups
                where c.Regions.Count > 1
                select c).ToList();

            var testunifiedCultureGroups = World.CultureGroups.Where(x => x.Unified).ToList();
            #endregion

            Console.WriteLine("### FILE GENERATION ###");
            debugWatch = Stopwatch.StartNew();
            var fileGenerator = FileGenerator.Instance();
            fileGenerator.Run();
            debugWatch.Stop();
            Console.WriteLine($"File Write Time: {debugWatch.ElapsedMilliseconds}ms");
        }
    }
}
