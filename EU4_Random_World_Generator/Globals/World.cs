﻿using System.Collections.Generic;
using EU4FlagLib;

namespace EU4_Random_World_Generator
{
    class World
    {
        public static List<CultureGroup> CultureGroups = new List<CultureGroup>();
        public static List<ReligionGroup> ReligionGroups = new List<ReligionGroup>();
        public static List<Tech> TechGroups = new List<Tech>();
        public static List<ReligionIcon> ReligionIcons = new List<ReligionIcon>();
        public static List<Government> Governments = new List<Government>();
        public static List<Country> Countries = new List<Country>();
        public static List<SuperRegion> SuperRegions = new List<SuperRegion>();
        public static List<CultureType> CultureTypes = new List<CultureType>();
        public static List<SuffixPair> SuffixPairs = new List<SuffixPair>();
        public static List<FlagComponents> FlagComponents = new List<FlagComponents>();
    }
}
