﻿namespace EU4_Random_World_Generator
{
    class Utils
    {
        /// <summary>
        /// Returns true or false based on a given chance of returning true
        /// </summary>
        /// <param name="probability">The probability of the value returning true</param>
        /// <returns>True of false</returns>
        public static bool GetProbability(int probability)
        {
            return R.Random.Next(0, 100) < probability;
        }
    }
}
