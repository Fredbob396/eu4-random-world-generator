﻿using System;

namespace EU4_Random_World_Generator
{
    /// <summary>
    /// 'R' stands for 'Resources'. All globl variables and configurable variables are stored in this class.
    /// </summary>
    class R
    {
        #region Readonly
        public static readonly string ModDirectory = $@"Generated_Mod_{DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}";
        #endregion

        #region Globals
        public static Random Random;
        #endregion

        #region Configurable
        public static string Seed;

        //ThinCountres()
        public static int MinPercentOfCountriesToPurge = 75;
        public static int MaxPercentOfCountriesToPurge = 95;

        //DetermineUnificationLeaders()
        public static int UnificationExemptionChance = 30;

        //SetProvinceOwned()
        public static int CapitalNamedAfterCountryChance = 25;

        //GetRandomGovernment()
        public static int TribalGovernmentChance = 50;

        //GetGovernmentFromList()
        public static int RepublicChance = 4;
        public static int TheocracyChance = 4;
        public static int VeryRareGovernmentChance = 2;
        public static int RareGovernmentChance = 15;

        //GetRandomGovernmentRank()
        public static int MinGovernmentRankChance = 15;

        //NameCultures()
        public static int PeopleNamesPerCultureGroup = 50;

        //CultureGroupOverlap()
        public static int CultureGroupOverlapChance = 15;

        //DetermineCultureUnification()
        public static int CultureUnificationChance = 10;

        //GetRandomCapitalDevelopment()
        public static int HighOwnedProvinceDevelopmentChance = 15;

        //GetRandomDevelopment()
        public static int MediumUnownedProvinceDevelopmentChance = 35;

        //GetLowDevelopment()
        public static int MinLowProvinceAdm = 1;
        public static int MaxLowProvinceAdm = 3;
        public static int MinLowProvinceDip = 1;
        public static int MaxLowProvinceDip = 3;
        public static int MinLowProvinceMil = 1;
        public static int MaxLowProvinceMil = 3;

        //GetMediumDevelopment()
        public static int MinMediumProvinceAdm = 3;
        public static int MaxMediumProvinceAdm = 7;
        public static int MinMediumProvinceDip = 3;
        public static int MaxMediumProvinceDip = 7;
        public static int MinMediumProvinceMil = 3;
        public static int MaxMediumProvinceMil = 5;

        //GetHighDevelopment()
        public static int MinHighProvinceAdm = 7;
        public static int MaxHighProvinceAdm = 15;
        public static int MinHighProvinceDip = 7;
        public static int MaxHighProvinceDip = 15;
        public static int MinHighProvinceMil = 5;
        public static int MaxHighProvinceMil = 10;

        //GeneratePersonName()
        public static int MalePrefixChance = 5;
        public static int MaleMiddleSyllableChance = 50;
        public static int FemalePrefixChance = 5;
        public static int FemaleMiddleSyllableChance = 50;
        public static int PlacePrefixChance = 5;
        public static int PlaceMiddleSyllableChance = 50;

        //ReligionGroupOverlap()
        public static int ReligionGroupOverlapChance = 15;

        //GenerateDenominations()
        public static int SuperRegionNewDenominationChance = 50;
        public static int RegionNewDenominationChance = 10;

        //GenerateHeretics()
        public static int MaxHereticsPerReligion = 5;

        //ReligionOverlap()
        public static int ReligionOverlapChance = 15;
        #endregion
    }
}
