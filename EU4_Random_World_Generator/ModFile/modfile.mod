name="@MODNAME"
path="mod/@MODNAME"
replace_path = "history/provinces"
replace_path = "localisation/prov_names_l_english"
replace_path = "localisation/prov_names_adj_l_english"
tags={
	"Map"
}
picture="nameofapicture.png"
supported_version="1.22.*.*"