﻿using System.Collections.Generic;

namespace EU4FlagLib
{
    public class Symbol
    {
        public List<string> Categories = new List<string>();
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
