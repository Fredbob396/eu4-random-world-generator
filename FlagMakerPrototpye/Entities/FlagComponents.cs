﻿using System.Collections.Generic;

namespace EU4FlagLib
{
    public class FlagComponents
    {
        public string Category { get; set; }
        public List<FlagPattern> FlagPatterns = new List<FlagPattern>();
        public List<Symbol> Symbols = new List<Symbol>();
        public CultureColors Colors = new CultureColors();
    }
}
