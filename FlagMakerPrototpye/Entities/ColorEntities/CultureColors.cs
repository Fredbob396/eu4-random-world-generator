﻿using System.Collections.Generic;

namespace EU4FlagLib
{
    public class CultureColors
    {
        public string Name;
        public int RareColorChance;
        public List<ColorGroup> ColorGroups = new List<ColorGroup>();
    }
}
