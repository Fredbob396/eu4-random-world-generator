﻿using System.Collections.Generic;
using System.Drawing;

namespace EU4FlagLib
{
    public class ColorGroup
    {
        public string Name;
        public bool IsRare;
        public List<ColorObject> ColorObjects = new List<ColorObject>();
        public List<Color> Colors = new List<Color>();
    }
}
