﻿using System.Drawing;

namespace EU4FlagLib
{
    public class Flag
    {
        public FlagPattern Pattern;
        public Symbol Symbol;
        public Color[] Colors;
    }
}
